/*
  input.h - Input
  Main purpose of this is to provide a way to keep track of what has been 
  read for the purposes of generating user-friendly error messages. If you
  are doing that, you may as well do a bit of buffering. I suspect most 
  Operating Systems don't inline system calls such as fgetc(), and function
  calls are surprisingly expensive. 

  Removing the position tracking code saves about 2kilobytes. Since this
  code is used quite heavily but the results are only needed if an error
  occurs, it is well worth disabling it for heavily-loaded servers
  where lack of line numbers in error messages is not an issue.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/



/* Initalises input fetching (input from give filename)
   Returns non-zeroif successful, and zero otherwise
*/
static __inline__ int initInputFile(Input_t *input, const char *filename)
{
memset(input, 0, sizeof(Input_t));
#ifndef NO_LINE_NUMBERS
input->posLast.line = 1;
#endif
input->fp = fopen(filename,"r");
if(input->fp == NULL)
   return 0;
input->length = fread(input->buffer,sizeof(char),INPUT_BUFFER_SIZE,input->fp);
return 1;
}


/* Cleans up input fetching mechanism
*/
static __inline__ void cleanupInput(Input_t *input)
{
if( input->fp != NULL )
   fclose(input->fp);
}


/* Gets (or peeks at) next character from input
   Returns EOF if no more input (or a read error occurred)
*/
static __inline__ int nextChar(Input_t *input, int peek)
{
int charVal;
if( input->offset == input->length )
   {
   if( input->fp == NULL )
      return EOF;
   input->length = 
      fread(input->buffer,sizeof(char),INPUT_BUFFER_SIZE,input->fp);
   if( feof(input->fp) )
      {
      fclose(input->fp);
      input->fp = NULL;
      }
   if( input->length == 0 )            
      return EOF;
   input->offset = 0;
   }
charVal = input->buffer[input->offset];
if( !peek )
   {
   input->offset++;
#ifndef NO_LINE_NUMBERS
   if( input->posLast.isNewline )
      {
      input->posLast.isNewline = 0;
      input->posLast.line++;
      input->posLast.col = 0;
      }
   else
      input->posLast.col++;
   if( charVal == '\n' || charVal == '\r' )
      input->posLast.isNewline = 1;
#endif
   }
return charVal;
}


/*****************************************************************************
 ** Input position storage. These are used when an error message needs to 
 ** point at a previously read position. More than one are defined as there 
 ** are cases where multiple positions need to be remembered.
 *****************************************************************************/

static __inline__ void storePos(Input_t *input)
{
#ifndef NO_LINE_NUMBERS
input->posStored.col = input->posLast.col;
input->posStored.line = input->posLast.line;
#endif
}

static __inline__ void storeStartPos(Input_t *input)
{
#ifndef NO_LINE_NUMBERS
input->posStart.col = input->posLast.col;
input->posStart.line = input->posLast.line;
#endif
}
