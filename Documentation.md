SSI.cgi: Standalone SSI interpretor
===================================

SSI.cgi is a standalone SSI (Server Side Includes) interpreter, intended for use
with lightweight webservers (such as Cherokee) that themselves do not support SSI
. It is implemented using C, with the intention of minimizing both overheads and
dependency requirements. 


## Background 
The project started when I was considering possible alternatives to
[Apache][apache], the webserver I have used in the past for both my own websites
and those I have setup for other people. I came across [Cherokee][cherokee], which
seemed to offer a good balance between being lightweight, ease of setup, and
features that are useful. In particular its ability to run scripts under different
Unix users didn't require the awkward setting up that ``suexec`` and ``suPHP``
needed. The big problem is that my (then) more recent sites used SSI to avoid
duplicating common page components such as headers. Although one possibility was
to convert the sites to use PHP instead, this was not something I wanted to do.
I also didn't want to use Perl, which was required by the SSI parsers I came
across on the web. In the end I decided to write my own parser, and SSI.cgi
was the result.


## Current status
Functionally SSI.cgi is no longer being developed, although it has had some
code clean-ups, most notably the stripping out of a lot of dead code that was
within comments. Aside from a quick fix to silence a compiler warning on modern
64-bit systems the program logic has not been changed for the last decade or
so. The version number has been bumped up to 1.9 but that was due to the
regeneration of the build configure scripts that still used my old university
email address.


## SSI Dialect
Below is a (semi-formal) grammar for directives. Internally ``isspace()`` and
``isalnum()`` are used for character classification.

```
    directive := "<--#" funcname space+ params space? "-->"
    params    := param (space+ param)*
    param     := key '=' quote value quote

    quote     := ['"`]
    funcname  := [a-z] | [A-Z] | [0123456789]
    key       := [a-z] | [A-Z] | [0123456789]
    value     := [a-z] | [A-Z] | [0123456789]
    space     := [\t\n\v\f\r ]
```
An example directive is:

```
    <!--#function param="value" param2="value" >;
```

## Supported directives

* **include** - Include a file
    - file
    : Included file is relative to the filesystem directory of current file.
    :  Note: There is currently no restruction on use of ```..``` in the
       file path - in future releases, they will be disallowed by default.
    - docroot
    : Included file is relative to the document root on the filesystem. Some
      SSI tutorials state this is the behaviour of the *virtual*
      parameter.
    - virtual
    : Included file is relative to URL of the document. This will trigger a
      fetch  request to the server, so the included files will need to be
      HTTP-accessible, at least for requests originating from the system
      hosting SSI.cgi. This approach is required as SSI.cgi is unaware of
      URL-to-filesystem mappings that may be in force on the server.
* **echo** - Display a parameter (or enviornment variable)
    - var
    :  Name of variable to print. Multiple var parameters may be included.
       Note that there are some [special variables](#special-echo-variables)
       provided by SSI.cgi.
    - encoding
    : Encoding to use when printing variable. The encoding affects
      all ``var=`` parameters between itself and either the next encoding
      parameter or the end of the echo command. Valid choices are
      *none*, *url* (encode for use in links), and *entity*
      (encode using HTML escape codes). Default is entity encoding.
* **flastmod** - Display datestamp of file
    - file
    - docroot
    - virtual *(not implemented yet)*
* **fsize** - Display file size
    - file
    - docroot
    - virtual *(not implemented yet)*
* **printenv** - Print all enviornment (if enabled) and user variables.
* **set** - Set user variable
    - var
    : Name of variable to set
    - value
    : What to set the variable to
* **config** - Set SSI configuration options
    - sizefmt
    : Format for displaying file sizes with fsize.
      Choice of *bytes* (default) which prints the exact size in
      bytes, and *abbrev* which postfixes the numbers with K, M
      or G if the files are in the kilobyte/megabyte/gigabyte region.
    - errmsg
    : SSI error message. Default is
      *There was an error processing this directive*.
    - timefmt
    : Format for flastmod timestamps. Uses strftime for time formatting.
      Default is *%d/%m/%Y %H:%M%:%*.
    - echomsg
    : Placeholder for undefined variables. Shown when echo is used with a
      nonexistant variable and the detailed error messages is turned off.
      Default is *undefined*.
* **if** - Conditional statement
    - expr
    : Conditional expression. [See below](#conditional-expressions) for format.
* **elif** - Conditional statement (else if)
    - expr
    : Conditional expression. [See below](#conditional-expressions) for format.
* **else** - Conditional statement
* **endif** - Conditional statement

## Conditional expressions

This is intended to be similar to the conditional expressions that
Apache's SSI module (``mod_include``) implements. The BNF grammer is as follows:
```
    alnum  := number | digit
    quote  := "'"
    string := alnum+ | (quote (!quote)* quote)+
    cond   := (string)+          |
              string '='  string |
              string '==' string |
              string '!=' string |
              string '<=' string |
              string '>=' string |
              string '<'  string |
              string '>'  string |
              ( cond )           |
              '!' cond           |
              cond '&&' cond     |
              cond '||' cond
```
A string on its own evaluates to true if it is non-empty
(i.e. not ```''```). '==' is an extension not present in
Apache, and ```<=``` & ```>;=``` use strcmp to decide
which string is 'smaller'. Whitespace outside of string is
ignored. Note that Boolean expressions (|| and &&) are
right-associative, so
```
    expr1 || expr2 || expr 3 || expr4
```
is treated as
```
    expr1 || (expr2 || (expr 3 || expr4))
```
which might not be how Apache does things (Apache's documentation doesn't
specify which associatively and it uses, and it simply advises people to
bracket defensively to avoid ambiguity when using both ``&&`` and ``||``.

NOTE: Apache's Regular Expression feature is not yet supported.

## Special echo variables

* **DATE_GMT** - Date/time (GMT)
* **DATE_LOCAL** - Date/time (local timezone)
* **DOCUMENT_NAME** - Document Name
* **DOCUMENT_URI** - Document address
* **LAST_MODIFIED** - Date file was last modified
* **QUERY_STRING_UNESCAPED** - Query String


## Unsupported directives

* **exec** - Execute command or script
    - *cgi*
    : Use *``<!--#include virtual="/cgi-bin/script.cgi"-->``* instead.
    - *cmd*
    : This was implemented, but because of problems with mixing fork()
      and POSIX Threads (used by FastCGI), it only worked with (non-fast)
      CGI. It is also a potential security hazard as there are no safety
      checks. The code is present, but it is not compiled by default.


## Running SSI.cgi

NOTE: FastCGI support was removed in v1.8

For the CGI version, the first parameter is the SSI file to parse, and
subsequent parameters are treated as option switches. The FastCGI version
does not have any required parameters and treats parameters as option
switches. The following option switches are accepted by both versions:

**``--rawinclude``**
:    Files included using file= or docroot= are not parsed for SSI directives,
     instead as simply copied verbatim. Potentially a major performance gain
     where SSI files are relativly small 'wrappers' that assemble large static
     files into a single page.

**``--nostderr``**
:    Error messages will not be sent to stderr. Useful if you are running
     SSI.cgi from the commandline, don't want SSI messages polluting
     your server logs, or your webserver doesn't handle stderr properly.

**``--errordetails``**
:    When an error occurs, users will see a detailed error message similar
     to what is sent to the server logs via stderr, rather than the normal
     generic "There was an error" message. Useful as seeing error messages
     via the web browser makes tracing errors easier, but it may be undesirable
     on production servers.

**``--allowdotdot``**
:    Allows the use of ``../`` within file and docroot parameters.
     A useful feature if you trust all your server's users.
     Disabled by default as Apache SSI seems to regard it as a
     security hazard.

The following option switches are only accepted by the (deprecated) FastCGI version:

**``--port PORT``**
:    Bind to given port, rather than expecting the webserver to
     have setup a socket for SSI.fcgi. If --interface is absent,
     ssi.fcgi will only allow connections from localhost.

**``--interface IP``**
:    Specify an interface to bind to. Requires --port to be specified

**``--onethread``**
:    Handle connections serially rather than forking each
     connection to its own thread. Assumes the server process
     creates multiple instances and load balances between them.


## Webserver setup

* [Cherokee](#cherokee)
* [Lighttpd](#lighttpd)

### Cherokee
To setup for use in [Cherokee][cherokee] use the phpcgi handler. Despite its
name, phpcgi is actually a generic generic interpreter interface:
```
    Extension shtml {
        Handler phpcgi {
            Interpreter /usr/local/bin/ssi.cgi
            }
        }
```
See Cherokee's [phpcgi documentation][cherokee-php] for further details. To use
FastCGI, use Cherokee's FastCGI handler, which is detailed in Cherokee's
[FastCGI documentation][cherokee-fcgi]:

```
    Extension shtml {
        Handler fcgi {
            Server localhost:9999 {
                Interpreter "/usr/local/bin/ssi.fcgi --port 9999"
            }
       }
```

### Lighttpd
For lighttpd you need to enable the cgi (or fastcgi module). This is done
by uncommenting the relevent part of *server.modules* in the lighttpd 
config file:
```
    server.modules = ( "mod_access","mod_fastcgi","mod_cgi","mod_accesslog" )
```
You then need to assign SSI.cgi as a 'handler' for SSI files. For standard
CGI this is quite easy:
```
    cgi.assign = ( ".shtml" => "/usr/local/bin/ssi.cgi" )
```
For FastCGI it is a similar directive:
```
    fastcgi.server =
        ( ".shtml" =>
                (( "host" => "127.0.0.1", "port" => 9999,
                   "bin-path" => "/usr/local/bin/ssi.fcgi"
                ))
        )
```

## Licence
IconSharp is licenced under [version 2 of the GPL][gpl2].


## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``


[apache]: http://httpd.apache.org/
[cherokee]: http://www.cherokee-project.com/
[cherokee-php]: http://www.cherokee-project.com/doc/PHP_execution.html
[cherokee-fcgi]: http://www.cherokee-project.com/doc/FastCGI.html
[gpl2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
