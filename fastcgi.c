/* 
  fastcgi.c - FastCGI interface
  I'm coming to the conclusion that multithreading this interface is
  not worthwhile. I suspect the cost of creating/destorying threads
  is not that different from the overheads associated with standard
  CGI, and there is no point in having multiple persistent threads
  internally as the webserver spawns multiple instances and load 
  balances between them.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h> /* // or #include </arpa/inet.h> */
#include <errno.h>
#include <unistd.h> /* close() */
#include <pthread.h>
#include <netdb.h>
#include <sys/time.h> /*// fd_set & select()*/
#define FASTCGI
#include "ssi.h"

typedef struct
   {
   unsigned char ver;
   unsigned char type;
   unsigned char requestId[2];
   unsigned char contentLen[2];
   unsigned char paddingLen;
   unsigned char unused;
} FCGI_RecordHeader_t;

typedef struct
  {
  int socket;
  int requestId;
} Session_t;


/*****************************************************************************
 ** Conversion between integers and byte arrays
 *****************************************************************************/
/* {{{ */

static __inline__ int bytesToInt16(const unsigned char *bytes)
{
return ((int)bytes[0])<<8 | ((int)bytes[1]);
}

static __inline__ int bytesToInt32(const unsigned char *bytes)
{
return ((int)bytes[0])<<24 | ((int)bytes[1])<<16 | 
   ((int)bytes[2])<<8 | ((int)bytes[3]);
}

static __inline__ void int32ToBytes(unsigned char *bytes, const int value)
{
bytes[0] = (value>>24) & 0xff;
bytes[1] = (value>>16) & 0xff;
bytes[2] = (value>>8 ) & 0xff;
bytes[3] = (value    ) & 0xff;
}

static __inline__ void int16ToBytes(unsigned char *bytes, const int value)
{
bytes[0] = (value>>8 ) & 0xff;
bytes[1] = (value    ) & 0xff;
}

/* }}} */


/*****************************************************************************
 ** Send various types of FastCGI record
 *****************************************************************************/
/* {{{ */

void sendEndRequest(int connection, int requestId, 
	       int appStatus, unsigned char protocolStatus)
{
/* {{{ */
FCGI_RecordHeader_t recordHdr;
unsigned char subpacket[8];
recordHdr.ver = 1;
recordHdr.type = 3; /* FCGI END_REQUEST */
int16ToBytes(recordHdr.requestId,requestId);
int16ToBytes(recordHdr.contentLen,8);
recordHdr.paddingLen = 0;
recordHdr.unused = 0;
int32ToBytes(&subpacket[0],appStatus);
subpacket[4] = protocolStatus;
send(connection, &recordHdr, sizeof(FCGI_RecordHeader_t),0);
send(connection, subpacket, 8, 0);
/* }}} */
}

void sendStdout(int connection, int requestId, const char *payload, int length)
{
/* {{{ */
FCGI_RecordHeader_t recordHdr = {1,6,    /* version, STDOUT */
				 {0,0},  /* requestId (2 bytes) */
				 {0,0},  /* contentLength (2 bytes) */
				 0,0};   /* padding, unused */
const char zeroes[8] = {0,0,0,0,0,0,0,0};

int16ToBytes(recordHdr.requestId, requestId);
int16ToBytes(recordHdr.contentLen,length);
recordHdr.paddingLen = length & 0x7;
send(connection, &recordHdr, sizeof(FCGI_RecordHeader_t), 0);
if( length )
   {
   send(connection, payload, length, 0);
   if( recordHdr.paddingLen )
      send(connection,zeroes,recordHdr.paddingLen,0);
   }
/* }}} */
}

void sendUnknownType(int connection, int requestId, unsigned char type)
{
/* {{{ */
FCGI_RecordHeader_t recordHdr = {1,11, /* ver 1, Unknown Type */
				 {0,0},{0,0},0,0};
unsigned char subpacket[8];
int16ToBytes(recordHdr.requestId,requestId);
int16ToBytes(recordHdr.contentLen,8);
subpacket[0] = type;
send(connection, &recordHdr, sizeof(FCGI_RecordHeader_t),0);
send(connection, subpacket, 8, 0);
/* }}} */
}


void sendStderr(int connection, int requestId, const char *payload, int length)
{
/* {{{ */
FCGI_RecordHeader_t recordHdr = {1,7,    /* version, STDOUT */
				 {0,0},  /* requestId (2 bytes) */
				 {0,0},  /* contentLength (2 bytes) */
				 0,0};   /* padding, unused */
const char zeroes[8] = {0,0,0,0,0,0,0,0};

int16ToBytes(recordHdr.requestId, requestId);
int16ToBytes(recordHdr.contentLen,length);
recordHdr.paddingLen = length & 0x7;
send(connection, &recordHdr, sizeof(FCGI_RecordHeader_t), 0);
if( length )
   {
   send(connection, payload, length, 0);
   if( recordHdr.paddingLen )
      send(connection,zeroes,recordHdr.paddingLen,0);
   }
/* }}} */
}

/* }}} */


/*****************************************************************************
 ** Interface for the output buffer (stuff sent to client). Using callbacks 
 ** is preferable to putting a load of interface-specific code in the output
 ** buffering module, and it is one place where it is preferable to break the
 ** 'avoid function calls' concept.
 *****************************************************************************/

void dispatchFuncStdout(FILE *fp, const char *buffer, int length)
{
Session_t *session = (Session_t*) fp;
sendStdout(session->socket, session->requestId, buffer, length);
}

void dispatchFuncStderr(FILE *fp, const char *buffer, int length)
{
Session_t *session = (Session_t*) fp;
sendStderr(session->socket, session->requestId, buffer, length);
}


/*****************************************************************************
 ** Handling of incoming FastCGI 'records' (packets)
 *****************************************************************************/

void parseSSI(State_t *state);
int setSSI(Options_t *, char *, char *, char *);
int setOptions(Options_t *options, char **argv, int argc, int*);
int setDefaultMessages(Options_t *options);


static __inline__ int handleRecord(Session_t *session, State_t *state)
{
/* {{{ */

int payloadLen, requestId, result;
unsigned char buffer[65535];
unsigned char *payload;
char *key, *value;
int keyLen,valueLen;
FCGI_RecordHeader_t recordHdr;
int i;

/* {{{ Get header & payload (if any) */

result = recv(session->socket,&recordHdr,8,MSG_WAITALL);
if( result <= 0 )
   {
   /* Also catches orderly closed connection (result==0) */
   fprintf(stderr,"ERROR: Connection lost (%s) %s:%i\n", 
	   strerror(errno), __FILE__,__LINE__);
   return 0;
   }
requestId  = bytesToInt16(&recordHdr.requestId[0]);
payloadLen = bytesToInt16(&recordHdr.contentLen[0]);
if( payloadLen &&
    (result=recv(session->socket,buffer,payloadLen,MSG_NOSIGNAL|MSG_WAITALL)) < 0 )
   {
   /* Also catches orderly closed connection (result==0) */
   fprintf(stderr,"ERROR: Connection lost (%s) %s:%i\n", 
	   strerror(errno), __FILE__,__LINE__);
   return 0;
   }
payload = &buffer[0];

/* }}} */

/*
  //printf("Ver:%i Type:%i Id:%i Len:%i padding:%i\n", 
  //       recordHdr.ver,recordHdr.type,
  //       requestId,payloadLen,recordHdr.paddingLen);
*/

switch(recordHdr.type)
   {
   /* KeyValue (Params) */
   case 4: /* top 7 bits only. if MSB set, 4 bytes instead.. readNetInt()..*/
      /* {{{ */
      /*
	//      keyLen = *payload;
	//      printf("Ver:%i Type:KeyVal Id:%i Len:%i padding:%i\n", 
	//	     recordHdr.ver,requestId,payloadLen,recordHdr.paddingLen);
      */

      if( payloadLen )
	 {
	 while( payloadLen > 0 )
	    {
	    /* {{{ Read in key/value pair */
	    keyLen = *payload;
	    if( keyLen & 0x80 )
	       {
	       *payload = *payload & 0x7f;
	       keyLen = bytesToInt32(payload);
	       payload = payload + 4;
	       }
	    else
	       payload++;
	    valueLen = *payload;
	    if( valueLen & 0x80 )
	       {
	       *payload = *payload & 0x7f;
	       valueLen = bytesToInt32(payload);
	       payload = payload + 4;
	       }
	    else
	       payload++;
	    //FIXME: Not checked for mem error
	    key = (char*) malloc( sizeof(char) * (keyLen+1) );
	    if(!key)
	       return 0;
	    memcpy(key,payload,keyLen);
	    key[keyLen] = 0;
	    value = (char*) malloc( sizeof(char) * (valueLen+1) );
	    if(!value)
	       {
	       free(key);
	       return 0;
	       }
	    memcpy(value,payload+keyLen,valueLen);
	    value[valueLen] = 0;
	    /* }}} */
	    listAddStr(&state->options->envvars,key,value);
	    payloadLen -= 2 + keyLen + valueLen;
	    payload = payload + keyLen + valueLen;
	    }
	 }
      else
	 {
	 /* In the specs, STDIN packets should now be fetched. However, 
	    Cherokee does not send any, and Lighttpd doesn't care that
	    it is not read. */

	 /* {{{ End of params (send response)*/
	 char *contenttype = "Content-type: text/html\r\n\r\n";
	 ListLink_t *filename = listFetch(&state->options->envvars,
					  "SCRIPT_FILENAME");
	 ListLink_t *fileURI  = listFetch(&state->options->envvars,
					  "REQUEST_URI");
	 ListLink_t *hostname = listFetch(&state->options->envvars,
					  "HTTP_HOST");
	 if( hostname && fileURI && filename && 
	     setSSI(state->options,(char*)filename->value,
		    (char*)hostname->value,(char*)fileURI->value) &&
	     initInputFile(&state->input, (char*)filename->value))
	    {
	    sendStdout(session->socket, session->requestId, 
		       contenttype, strlen(contenttype));
	    state->output->flushFunc = &dispatchFuncStdout;
	    state->output->fp = (FILE*)session;
	    state->errors->flushFunc = &dispatchFuncStderr;
	    state->errors->fp = (FILE*)session;
	    initOutput(state->output);
	    initOutput(state->errors);
	    parseSSI(state);
	    flushOutput(state->output);
	    flushOutput(state->errors);
	    }
	 else
	    {
	    char *msg = "Status 403 Forbidden\r\n";
	    sendStdout(session->socket, session->requestId, msg, strlen(msg));
	    }
 
	 sendStdout(session->socket, session->requestId, 0, 0);
	 sendEndRequest(session->socket, session->requestId, 0, 0);
	 cleanupInput(&state->input);
	 return 0;
	 /* }}} */
	 }
      break;
      /* }}} */
      
   case 1: /* Begin Request */
      /* {{{ */

      keyLen = bytesToInt16(payload);
      /*
	printf("Type:BeginReq Id:%i Len:%i pad:%i\n role:%i (keepflag:%i)\n",
	 requestId,payloadLen,recordHdr.paddingLen,keyLen,payload[2]);
      */
      if( session->requestId != -1 )
	 {
	 sendEndRequest(session->socket,requestId,0,1); 
	 /* 1=cannot multiplex conn */
	 return 0;
	 }
      else
	 switch( keyLen )
	    {
	    case 1: /* Responder */
	       session->requestId = requestId;
	       listReset(&state->options->envvars,1);
	       listReset(&state->options->uservars,1);
	       listReset(&state->param,1);
	       break;  
	    case 2: /* Authoriser */
	    case 3: /* Filter */
	    default:		  
	       sendEndRequest(session->socket,requestId,0,3);
	       /* 3=UNKNOWN_ROLE */
	       return 0;
	    }
      break;

      /* }}} */

   case 2: /* Abort Request */
      return 0;

   case 5: /* Stdin */
      /*FIXME: Implement Stdin reading. It is used for URL params */

   
   case 8: /* Data  */
      /*FIXME: Data record type actually used?*/
 
  case 9: /* Get Values (return record type: 10, keyval pairs as PARAMS) */
      /*FIXME: Implement queries of FastCIG app by server */
      if( payloadLen )
	 {
	 while( payloadLen > 0 )
	    {
	    /* {{{ Read in key/value pair */

	    keyLen = *payload;
	    if( keyLen & 0x80 )
	       {
	       *payload = *payload & 0x7f;
	       keyLen = bytesToInt32(payload);
	       payload = payload + 4;
	       }
	    else
	       payload++;
	    valueLen = *payload;
	    if( valueLen & 0x80 )
	       {
	       *payload = *payload & 0x7f;
	       valueLen = bytesToInt32(payload);
	       payload = payload + 4;
	       }
	    else
	       payload++;
	    key = (char*) malloc( sizeof(char) * (keyLen+1) );
	    if(!key)
	       return 0;
	    memcpy(key,payload,keyLen);
	    key[keyLen] = 0;
	    value = (char*) malloc( sizeof(char) * (valueLen+1) );
	    if(!value)
	       {
	       free(key);
	       return 0;
	       }
	    memcpy(value,payload+keyLen,valueLen);
	    value[valueLen] = 0;
	    printf(">>%s %s\n", key, value);
	    payloadLen -= 2 + keyLen + valueLen;
	    payload = payload + keyLen + valueLen;

	    /* }}} */
	    }
	 }
      /*
	// Max TCP connects
	if( strncmp("FCGI_MAX_CONNS",key,0x0e) == 0 )
	printf("\x00eFCGI_MAX_CONNS\x0011");
	// Max concurrent requests
	else if( strncmp("FCGI_MAX_REQS",key,0x0d) == 0 )
	printf("\x00dFCGI_MAX_REQS\x0001");
	// Connections multiplexable
	else if( strncmp("FCGI_MAX_CONNS",key,0x0f) == 0 )      
	printF("\x00fFCGI_MPXS_CONNS\x0010");
      */

   default:
      /* {{{ Unknown type */
      printf("Unrecognised record  "
	     " Ver:%i Type:%i Id:%i Len:%i padding:%i\n", 
	     recordHdr.ver,recordHdr.type,requestId,payloadLen,
	     recordHdr.paddingLen);
      for(i=0;i<payloadLen;i++)
	 printf(" %03i",buffer[i]);
      printf("\n");
      sendUnknownType(session->socket, requestId, recordHdr.type);
      /* }}} */
      break;
   }

/* {{{ Read (and discard) any padding */
if( recordHdr.paddingLen )
   {
   if( recv(session->socket,buffer,recordHdr.paddingLen,MSG_WAITALL) < 0 )
      {
      fprintf(stderr,"ERROR: Connection lost (%s) %s:%i\n", 
	      strerror(errno), __FILE__,__LINE__);
      return 0;
      }      
   }
/* }}} */
return 1;

/* }}} */
}


/*****************************************************************************
 ** Handle client connection. Each connection is hived off to its own thread.
 ** Might be better to use Unix fork() rather than pthreads, as fork() is 
 ** needed for the #exec directive, and it doesn't mix well with Pthreads..
 *****************************************************************************/

void *handleClient(void *connection)
{
/* {{{ */

Options_t options;
State_t state;
Output_t outputs[2];
Session_t session;

state.output = &outputs[0];
state.errors = &outputs[1];
memcpy(&options, (Options_t*)((int*)connection)[1], sizeof(Options_t));
state.options = &options;
if(!listInit(&state.param)    || !listInit(&options.uservars) || 
   !listInit(&options.envvars)|| !setDefaultMessages(&options))
    {
    fprintf(stderr,"SSI.cgi: "MALLOC_ERROR_MSG"\n");
    return NULL;
    } 
session.socket = ((int*)connection)[0];
session.requestId = -1;
while( handleRecord(&session,&state) )
   {
   }
listDelete(&state.param,1);
listDelete(&options.uservars,1);
listDelete(&options.envvars,1);
close(session.socket);

/*FIXME: Is this atomic?*/
write(((int*)connection)[3], "exit", 5);

free( connection );
return NULL;

/* }}} */
}


/*****************************************************************************
 ** Program Entrypoint
 *****************************************************************************/

#define LOCALHOST 0x7f000001

/*//int setupTCP(INADDR_ANY,9999);*/

int setupTCP(int interface, int port)
{
/* {{{ */
int srvSocket;
struct sockaddr_in srvAddr;
/*//setsockopt(srvSocket,SOL_SOCKET,SO_REUSEADDR*/
srvSocket = socket(PF_INET,SOCK_STREAM,0);
memset(&srvAddr, 0, sizeof(struct sockaddr_in));
srvAddr.sin_family = PF_INET;
srvAddr.sin_port = htons(port);
srvAddr.sin_addr.s_addr = htonl(interface);
if(bind(srvSocket, (struct sockaddr *)&srvAddr, sizeof(struct sockaddr)) == -1)
   {
   fprintf(stderr,"FATAL: bind(%i) failed\n",port);
   exit(21);
   }
if( listen(srvSocket,0) == -1 )
   {
   fprintf(stderr,"FATAL: listen() failed\n");
   exit(1);
   }
return srvSocket;

/* }}} */
}

int setOptionsFCGI(Options_t *options, char **argv, int argc, 
		   int *port, int *interface)
{
/* {{{ */

int idx;
options->parseIncludeFile = 0;
options->noWarnings  = 0; /*// not used?*/
options->noLogErrors = 1;
options->logErrorsToUser = 1;
options->fSizeFmt = 0;
options->allowDotDot = 0;
options->oneThread = 0;
for(idx=0;idx<argc;idx++)
   {
   if( compareStrings("--rawinclude",argv[idx])==0 )
      options->parseIncludeFile = 0;
   else if( compareStrings("--nostderr",argv[idx])==0 )
      options->noLogErrors = 0;
   else if( compareStrings("--noerrordetails",argv[idx])==0 )
      options->logErrorsToUser = 0;
   else if( compareStrings("--allowdotdot",argv[idx])==0 )
      options->allowDotDot = 0;
   else if( compareStrings("--onethread",argv[idx])==0 )
      options->allowDotDot = 0;
   else if(compareStrings("--port",argv[idx])==0)
      {
      idx++;
      if( idx==argc )
	 {
	 fprintf(stderr,"SSI.cgi: --port is missing port number\n");
	 return 0;
	 }
      (*port) = atoi(argv[idx]);
      if( *port < 1 )
	 {
	 fprintf(stderr,
		 "SSI.cgi: --port only takes positive numbers, not '%s'\n",
		 argv[idx]);
	 return 0;
	 }
      }
   else if(compareStrings("--interface",argv[idx])==0)
      {
      struct hostent *ipAddr;
      idx++;
      if( idx==argc )
	 {
	 fprintf(stderr,"SSI.cgi: --interface is missing IP address\n");
	 return 0;
	 }
      ipAddr = gethostbyname(argv[idx]);
      if( ipAddr == NULL )
	 {
	 fprintf(stderr,
		 "SSI.cgi: Unable to resolve %s\n",argv[idx]);
	 return 0;
	 }
      (*interface) = 
	 ipAddr->h_addr[0] << 24 | ipAddr->h_addr[1] << 16 |
	 ipAddr->h_addr[2] << 8  | ipAddr->h_addr[3] ;
      if( *port == -1 )
	 *port = -2;
      }
   else
      {
      fprintf(stderr,"SSI.cgi: Unknown option %s\n",argv[idx]);
      return 0;
      }
   }
return 1;

/* }}} */
}


static __inline__ void fatalError(const char *str)
{
fprintf(stderr,str);
exit(1);
}

void sigSet(int *IPCpipe);

int main(int argc, char *argv[])
{
/* {{{ */
int srvSocket = 0;
Options_t options;
int port = -1;
int interface = LOCALHOST;
int clientCount = 0;
int IPCpipe[2];
char IPCbuffer[8];

State_t state;
Output_t outputs[2];
Session_t session;

if( !setOptionsFCGI(&options, &argv[1], argc-1, &port, &interface))
   return 1;
if( port == -2 )
   fatalError("USAGE: --interface also requires --port\n");
if( pipe(IPCpipe) )
   fatalError("FATAL: Error setting up internal pipe\n");
   
sigSet(&IPCpipe[0]);

if( port != -1 )
   srvSocket = setupTCP(interface,port);
else
   {
   /* This technique is suggested by the FastCGI specs to see if the
      program was spawned by a complaint webserver. I don't know if
      it is as reliable as claimed, or portable.  */
   struct sockaddr info;
   unsigned int infoSize = sizeof(struct sockaddr);
   int result = getpeername(srvSocket, &info, &infoSize);
   if( result != -1 || errno != ENOTCONN)
      fatalError(
	 "No open socket from webserver. Try using the --port parameter\n");
   }

if(options.oneThread)	 
   {
   state.output = &outputs[0];
   state.errors = &outputs[1];
   state.options = &options;
   if(!listInit(&state.param)    || !listInit(&options.uservars) || 
      !listInit(&options.envvars)|| !setDefaultMessages(&options))
      {
      fprintf(stderr,"SSI.cgi: "MALLOC_ERROR_MSG"\n");
      return 1;
      } 
   }

/* Server loop */
while(1)
   {
   int result;
   fd_set sockSet;
   FD_ZERO(&sockSet);
   FD_SET(srvSocket,&sockSet);
   FD_SET(IPCpipe[0],&sockSet);
   result = select(srvSocket>IPCpipe[0] ? srvSocket+1 : IPCpipe[0]+1, 
		   &sockSet,0,0,0);
   //  printf(">%i  %i %i %i %i  %i\n", result, EBADF,EINTR,EINVAL,ENOMEM, errno);
   if( result > 0 && FD_ISSET(srvSocket,&sockSet) )
      {
      /* {{{ Incoming client connection */
      int cliSocket = accept(srvSocket,NULL,NULL);
      /* Assumes all pointers are same size. */
      int *cliSocketPtr = (int*)malloc(sizeof(int*)<<2);
      if(!options.oneThread)
	 {
	 /* {{{ Multithreaded server */
	 pthread_t thread;
	 if(!cliSocketPtr)
	    {
	    fprintf(stderr,"SSI.fcgi: "MALLOC_ERROR_MSG"\n");
	    close(cliSocket);
	    break;
	    }
	 else
	    {
	    cliSocketPtr[0] = cliSocket;
	    cliSocketPtr[1] = (int)&options;
	    cliSocketPtr[2] = (int)&clientCount;
	    cliSocketPtr[3] = IPCpipe[1];
	    clientCount++;
	    if( pthread_create(&thread, NULL, &handleClient, cliSocketPtr) )
	       {
	       fprintf(stderr,"SSI.fcgi: Thread forking failed\n");
	       close(cliSocket);
	       clientCount--;
	       break;
	       }
	    }
	 /* }}} */
	 }
      else
	 {
	 /* {{{ Single-threaded server */
	 session.socket = cliSocket;
	 session.requestId = -1;
	 while( handleRecord(&session,&state) );
	 listReset(&state.param,1);
	 listReset(&options.uservars,1);
	 listReset(&options.envvars,1);
	 close(session.socket);
	 /* }}} */
	 }
      /* }}} */
      }
   if( (result > 0 && FD_ISSET(IPCpipe[0],&sockSet)) ||
       (result == -1 && errno == EINTR ))
      {
      /* {{{ Incoming message from child thread (or signal caught) */
      char buff[5];
      read(IPCpipe[0],buff, 5);
      printf("SIG: %s\n",buff);
      if( strncmp("exit",buff,4) == 0 )
	 clientCount--;
      else if( strncmp("down",buff,4) == 0 )
	 break;
      else
	 break;
      /* }}} */
      }
   }
close(srvSocket);
if(options.oneThread)
   {
   listDelete(&state.param,1);
   listDelete(&options.uservars,1);
   listDelete(&options.envvars,1);
   }

/* Wait for other threads to terminate. */
while(clientCount > 0 )
   {
   read(IPCpipe[0],IPCbuffer, 5);
   if( strncmp("exit",IPCbuffer,4) == 0 )
      clientCount--;
   else
      {
      fprintf(stderr,"Unknown IPC msg: %s\n",IPCbuffer);
      break;
      }
   }
close(IPCpipe[0]);
close(IPCpipe[1]);
      
return 0;
/* }}} */
}


