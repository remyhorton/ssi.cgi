/*
  cgi.c - CGI interface

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "ssi.h"
/*//extern char *malloc_options = "A";*/


/******************************************************************************
 ** SSI Functions
 *****************************************************************************/

int setOptions(Options_t *options, char **argv, int argc)
{
/* {{{ */
int idx;

options->parseIncludeFile = -1; /* 2 = base on extension */
options->noWarnings       = 0; /* not used? */
options->noLogErrors      = 1;
options->logErrorsToUser  = 1;
options->fSizeFmt         = 0;
options->allowDotDot      = 0;

if( !listInit(&options->SSIextensions) )
    {
    fprintf(stderr,MALLOC_ERROR_MSG);
    return 0;
    }
for(idx=0;idx<argc;idx++)
   {
   if( compareStrings("--rawinclude",argv[idx])==0 )
       {
       if( options->parseIncludeFile == 2 )
	   {
	   fprintf(stderr,"SSI.cgi: Cannot mix --rawinclude and --ext");
	   return 0;
	   }
       options->parseIncludeFile = 0;
       }
   else if( compareStrings("--nostderr",argv[idx])==0 )
      options->noLogErrors = 0;
   else if( compareStrings("--noerrordetails",argv[idx])==0 )
      options->logErrorsToUser = 0;
   else if( compareStrings("--allowdotdot",argv[idx])==0 )
      options->allowDotDot = 0;

   else if( compareStrings("--ext",argv[idx])==0 )
       {
       char *extension;
       idx++;
       if( options->parseIncludeFile == 0 )
	   {
	   fprintf(stderr,"SSI.cgi: Cannot mix --rawinclude and --ext");
	   return 0;
	   }
       options->parseIncludeFile = 2;
       if( idx==argc )
	   fprintf(stderr,"SSI.cgi: --ext without specified extension");
       else
	   {
	   extension = (char*) malloc( sizeof(char)*(strlen(argv[idx])+1) );
	   if( extension == NULL )
	       {
	       fprintf(stderr,MALLOC_ERROR_MSG);
	       return 1;
	       }
	   strcpy(extension,argv[idx]);
	   listAddStr(&options->SSIextensions,extension,NULL);
	   }
       }

   else
      {
      fprintf(stderr,"SSI.cgi: Unknown option %s\n",argv[idx]);
      return 0;
      }
   }

if( options->parseIncludeFile == -1 )
    options->parseIncludeFile = 1;

return 1;
/* }}} */
}

void parseSSI(State_t *state);
int setSSI(Options_t *, char *, char *, char *);
int setDefaultMessages(Options_t *options);

extern char **environ;
/* Program entrypoint
*/
int main(int argc, char *argv[])
{
State_t state;
Options_t options;
Output_t outputs[2];
int idx = 0;

if( argc < 2 )
    {
    fprintf(stderr,"SSI.cgi: No SSI file given\n");
    return 1;
    }
if( !initInputFile(&state.input, argv[1]) )
    {
    fprintf(stderr,"SSI.cgi: Error opening SSI file %s\n",argv[1]);
    return 1;
    }
if( !setDefaultMessages(&options) ||
    !setSSI(&options,argv[1],getenv("HTTP_HOST"),getenv("REQUEST_URI")))
    {
    fprintf(stderr,"SSI.cgi: "MALLOC_ERROR_MSG"\n");
    return 1;
    }
if( !setOptions(&options,&argv[2],argc-2) )
   return 1;

state.options = &options;
if(!listInit(&state.param) ||
   !listInit(&options.uservars) ||
   !listInit(&options.envvars) )
    {
    fprintf(stderr,MALLOC_ERROR_MSG);
    return 1;
    }

/* {{{ Load enviornment variables */
while( environ[idx] )
   {
   int offset = 0;
   char *key;
   char *value;
   while( environ[idx][offset] != '=' )
      offset++;
   key   = (char*) malloc( sizeof(char)*(offset+1) );
   value = (char*) malloc( sizeof(char)*(strlen(&environ[idx][offset+1])+1) );
   if( key==NULL || value==NULL )
      {
      fprintf(stderr,MALLOC_ERROR_MSG);
      return 1;
      }
   strncpy(key,environ[idx], offset);
   strcpy(value,&environ[idx][offset+1]);
   listAddStr(&options.envvars,key,value);
   idx++;
   }
/* }}} */

state.output = &outputs[0];
state.errors = &outputs[1];
state.output->fp = stdout;
state.errors->fp = stderr;
initOutput(state.output);
initOutput(state.errors);

outputString(state.output,"Content-type: text.html\n\n");
parseSSI(&state);

flushOutput(state.output);
flushOutput(state.errors);
cleanupInput(&state.input);
listDelete(&state.param,1);
listDelete(&options.uservars,1);
listDelete(&options.envvars,1);

return 0;
}
