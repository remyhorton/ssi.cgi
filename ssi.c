/*
  ssi.c - SSI processing (mostly parsing)
  This contains all the routines for parsing of the SSI file. It is a resursive
  descent parser, and being by far the oldest piece of code there are a few
  places where the hacking in of new features is noticeable.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include "ssi.h"


/******************************************************************************
 ** SSI Function table
 *****************************************************************************/

void funcNOP(State_t *state);
void funcInclude(State_t *state);
void funcExec(State_t *state);
void funcEcho(State_t *state);
void funcConfig(State_t *state);
void funcFiledate(State_t *state);
void funcFilesize(State_t *state);
void funcPrintenv(State_t *state);
void funcSet(State_t *state);
void funcNOP(State_t *state);
void funcIf(State_t *state);
void funcElseIf(State_t *state);
void funcElse(State_t *state);
void funcEndIf(State_t *state);


struct 
  {
  char *name;
  int numParams;
  void (*func)(State_t *);
  char *validParams[5];
} functions[] = 
  {
     {"include",1, &funcInclude,  {"virtual","file","docroot",NULL} },
     {"exec",   1, &funcExec,     {"cgi","cmd",NULL} },
     {"echo",   -1,&funcEcho,     {"var","encoding",NULL} },
     {"config", 1, &funcConfig,   {"sizefmt","errmsg","timefmt","echomsg",
				   NULL}},
     {"flastmod",1,&funcFiledate, {"virtual","file","docroot",NULL} },
     {"fsize",1,   &funcFilesize, {"virtual","file","docroot",NULL} },
     {"printenv",0,&funcPrintenv, {"virtual","file",NULL} },
     {"set",     2,&funcSet,      {"var","value",NULL} },

     {"if",      1, &funcIf,      {"expr",NULL}},
     {"elif",    1, &funcElseIf,  {"expr",NULL}},
     {"else",    0, &funcElse,    {NULL}},
     {"endif",   0, &funcEndIf,   {NULL}},

     {0,         0,&funcNOP,      {0}}
  };


/*****************************************************************************
 ** SSI options and commandline parsing
 *****************************************************************************/
/* {{{ */

int setSSI(Options_t *options, char *filename,char *hostname, char *requestURI)
{
char *FilePath;
int FilePathLength;
int URIlength;
int offsetURIpath;
char *URIpath;
char *URI;

/* {{{ Extract filename and filesystem path */

FilePathLength = strlen(filename);
FilePath = (char *) malloc( sizeof(char) * (FilePathLength+1) );
if(FilePath == NULL)
    return 0;
strcpy(FilePath,filename);
while( FilePathLength > 0 && FilePath[FilePathLength-1] != '/')
    FilePathLength--;
/*FIXME: FilePathLength==0*/
options->fileName = copyStr(&FilePath[FilePathLength]);
if( options->fileName == NULL )
   {
   free(FilePath);
   return 0;
   }
FilePath[FilePathLength] = 0;
options->basePath = FilePath;

/* }}} */

/* {{{ extract URI hostname and path */
/* FIXME: Check this */
URIlength = 16;
if( hostname )
   URIlength += strlen(hostname);
if( requestURI )
   URIlength += strlen(requestURI);
URIpath = (char *) malloc( sizeof(char) * (URIlength+1) );
if(URIpath == NULL )
    {
    free(FilePath);
    free(options->fileName);
    return 0;
    }
URI=URIpath;
/* FIXME: Allow for https? */
strcpy(URIpath,"http://");
URIpath = &URIpath[ strlen(URIpath) ];
URIpath[0] = 0;
if( hostname )
   strcpy(URIpath,hostname);
offsetURIpath = strlen(URI);
URIpath = &URIpath[ strlen(URIpath) ];
/* FIXME: Always has leading/trailing '/'? */
if( requestURI )
   strcpy(URIpath,requestURI);
URIlength = strlen(URIpath);
while( URIlength > 0 && URIpath[URIlength-1] != '/')
    {
    URIpath[URIlength-1] = 0;
    URIlength--;
    }
options->baseURI = URI;
options->offsetURIpath = offsetURIpath;
/* }}} */

return 1;
}

int setDefaultMessages(Options_t *options)
{
options->errorMsg = copyStr(DEFAULT_ERROR_MESSAGE);
options->echoMsg = copyStr(DEFAULT_ECHO_MESSAGE);
options->timeFmt = copyStr(DEFAULT_TIME_FORMAT);
if(!options->errorMsg || !options->echoMsg || !options->timeFmt)
   {
   if( options->errorMsg ) free(options->errorMsg);
   if( options->echoMsg )  free(options->echoMsg);
   if( options->timeFmt )  free(options->timeFmt);
   return 0;
   }
return 1;
}

/* }}} */


/******************************************************************************
 ** Error printing
 *****************************************************************************/
/* {{{ */

/* Does actual printing of error messages (the functions below are all
   wrappers that choose different reference positions). A detailed error
   message is sent to stderr (for logging) and a generic one is sent to
   stdout (for sending to the client).
*/
static void parseErrorBody(State_t *state,
#ifndef NO_LINE_NUMBERS
				  int line, int col,
#endif
				  char *str, va_list args)
{
char buffer[512];
#ifndef NO_LINE_NUMBERS
sprintf(buffer,"SSI.exe Error (%i:%i): ",line,col);
#else
sprintf(buffer,"SSI.exe Error: ");
#endif
vsprintf(&buffer[strlen(buffer)],str,args);

if( !state->options->noLogErrors )
   {
   outputPrintf(state->errors,"%s\n",buffer);
   }
if( state->options->logErrorsToUser )
   {
   outputChar(state->output,'[');
   outputString(state->output,buffer);
   outputChar(state->output,']');
   }
else
   outputString(state->output,state->options->errorMsg);
}


/* Print error message using 'last fetched' position.
   Wrapper around errorPrintMsg()
*/
static void parseError(State_t *state, char *str, ...)
{
va_list args;
va_start(args,str);
parseErrorBody(state,
#ifndef NO_LINE_NUMBERS
	       state->input.posLast.line,
	       state->input.posLast.col,
#endif
	       str,args);
va_end(args);
}


/* Print error message using 'last stored' position.
   Wrapper around errorPrintMsg()
*/
static void parseErrorStoredPos(State_t *state, char *str, ...)
{
va_list args;
va_start(args,str);
parseErrorBody(state,
#ifndef NO_LINE_NUMBERS
	       state->input.posStored.line,
	       state->input.posStored.col,
#endif
	       str,args);
va_end(args);
}

/* }}} */

/******************************************************************************
 ** Parsing functions
 *****************************************************************************/
/* {{{ */

/* Reads and discards all whitespace until a non-whitespace character
   is found
*/
static void __inline__ parseEatSpace(State_t *state)
{
while( isspace( nextChar(&state->input,1) ) )
   nextChar(&state->input,0);
}


/* Consumes (and discards) chars until either "-->" is found, or EOF.
   I have doubts about the efficency of the algorithm used..
   Not inlined, as inlining it uses 3kb. Not worth it for error handlers
*/
static void errorFindClose(State_t *state, const char *endTag)
{
int tagLength = strlen(endTag);
int i;
int buf[10];
assert(tagLength < 10);

for(i=0; i<tagLength; i++)
   if( (buf[i] = nextChar(&state->input,0)) == EOF )
      return;

/* I dislike goto */
for(i=0; i<tagLength; i++)
   {
   if( buf[i] != endTag[i] )
      {
      for(i=1; i<tagLength; i++)
	 buf[i-1] = buf[i];
      buf[i-1] = nextChar(&state->input,0);
      if( buf[i] == EOF )
	 break;
      i = -1;
      }
   }
return;
}



/* Look for expected close tag.
   When this function is called, a key/value pair has already been eliminated
   as a possibility, but if not found the error message has to report it as
   one of the expected alternatives. Silently ignoring errors at this point
   by using the similar errorFindClose() function is simpler, but it is
   also not correct.
*/
static __inline__ int nonerrorFindClose(State_t *state, const char *endTag)
{
int tagLength = strlen(endTag);
int i;
int buf[10];
int noerror = 1;
assert(tagLength < 10);

storePos(&state->input);
for(i=0; i<tagLength; i++)
   if( (buf[i] = nextChar(&state->input,0)) == EOF )
      {
      parseError(state,
		 "Reached end of file looking for '%s'"
		 " or key=value pair", endTag);
      return 0;
      }

/* I dislike goto */
for(i=0; i<tagLength; i++)
   {
   if( buf[i] != endTag[i] )
      {
      if( !noerror )
	 {
	 parseErrorStoredPos(state, "Expected '%s' or key=value pair", endTag);
	 noerror = 0;
	 }
      for(i=1; i<tagLength; i++)
	 buf[i-1] = buf[i];
      buf[i-1] = nextChar(&state->input,0);
      if( buf[i-1] == EOF )
	 {
	 parseError(state,
		    "Reached end of file looking for '%s'"
		    " or key=value pair", endTag);
	 return 0;
	 }
      i = -1;
      }
   }
return noerror;
}




#define SUBS_TEXT    1
#define SUBS_SIGEL   2
#define SUBS_BRACE   3
#define SUBS_NAME    4
#define SUBS_ENDNAME 5
#define SUBS_BAILOUT 6

/* Directive parameter substitution. It goes thru the parameter replacing
   placeholder variable names ($varname and ${varname} with the relevent
   values). Its implemented as a basic state machine, and doesn't
   differentiate between different types of errors. Ideally it ought to
   at least seperate parse errors and malloc errors..
 */
char *substitute(State_t *state, char *input)
{
/* {{{ */
StringBuffer_t output,varname;
int fsm = SUBS_TEXT;
ListLink_t *name;
if( !initStringBuffer(&output) )
   return NULL;
if( !initStringBuffer(&varname) )
   {
   free( getStringBufferString(&output) );
   return NULL;
   }

while( *input )
   {
   switch( fsm )
      {
      case SUBS_TEXT:
	 if( *input == '$' )
	    fsm = SUBS_SIGEL;
	 else
	    appendStringBuffer(&output,*input);
	 input++;
	 break;

      case SUBS_SIGEL:
	 if( *input == '{' )
	    {
	    fsm = SUBS_BRACE;
	    input++;
	    }
	 else if( isalnum(*input) )
	    {
	    fsm = SUBS_NAME;
	    input--;
	    }
	 else
	    fsm = SUBS_BAILOUT;
	 break;

      case SUBS_BRACE:
	 if( *input == '{' )
	    fsm = SUBS_BAILOUT;
	 else if( *input == '}' )
	    {
	    fsm = SUBS_ENDNAME;
	    }
	 else if( isalnum(*input) )
	    {
	    appendStringBuffer(&varname,*input);
	    input++;
	    }
	 else
	    fsm = SUBS_BAILOUT;
	 break;

      case SUBS_NAME:
	 if( isalnum(*input) )
	    {
	    appendStringBuffer(&varname,*input);
	    input++;
	    }
	 else
	    {
	    appendStringBuffer(&varname,*input);
	    fsm = SUBS_ENDNAME;
	    }
	 break;

      case SUBS_ENDNAME:
	 name = listFetch(&state->options->envvars,
			 getStringBufferString(&varname));
	 if( name == NULL )
	    name=listFetch(&state->options->uservars,
			  getStringBufferString(&varname));
	 if( name == NULL )
	     fsm = SUBS_BAILOUT;
	 else
	    {
	    char *value = (char*)name->value;
	    while( *value )
	       {
	       appendStringBuffer(&output,*((char*)name->value));
	       value++;
	       }
	    resetStringBuffer(&varname);
	    fsm = SUBS_TEXT;
	    input++;
	    }
	 break;

      case SUBS_BAILOUT:
      default:
	 /* Treat all errors the same */
	 free( getStringBufferString(&output) );
	 free( getStringBufferString(&varname) );
	 return NULL;
      }
   }
return getStringBufferString(&output);
/* }}} */
}



/* Parses a Key/Value pair
   Returns 1 if successful and 0 if not. If unsuccessful, the calling
   function should clean up.
 */
static __inline__ int parseKeyValue(State_t *state, int functionIdx)
{
char *key;
char *val;
char *rawVal;
StringBuffer_t buffer;
int i = 0;
char quotemark;

/* Key */
if(!initStringBuffer(&buffer))
    {
    parseError(state,MALLOC_ERROR_MSG);
    return 0;
    }
while( isalnum( nextChar(&state->input,1) ) )
   {
   if(!appendStringBuffer(&buffer,nextChar(&state->input,0)))
       {
       parseError(state,MALLOC_ERROR_MSG);
       return 0;
       }
   if(!i)
      {
      storePos(&state->input);
      i=1;
      }
   }
key = getStringBufferString(&buffer);

/* = */
if( nextChar(&state->input,0) != '=' )
    {
    parseError(state,"Expected '='");
    return 0;
    }

/* " <anything> " */
quotemark = nextChar(&state->input,0);
if( quotemark != '"' && quotemark !='\'' && quotemark != '`')
   {
   parseError(state,"Expected \" ' or `");
   return 0;
   }

/* Consider all text until the next quote to be part of the value.
   quote mark has to be same as opening one, so " 'x' = `y` " is all
   one value ( 'x' = `y` )..
   FIXME: Make use of ` and ' non-default, as it conflicts with conditionals */
if(!initStringBuffer(&buffer))
    {
    parseError(state,MALLOC_ERROR_MSG);
    return 0;
    }
while( nextChar(&state->input,1) != quotemark )
   {
   int c = nextChar(&state->input,0);
   if( c == EOF )
      {
      parseError(state,"Hit end of file looking for close quote");
      return 0;
      }
   if(!appendStringBuffer(&buffer,c))
       {
       parseError(state,MALLOC_ERROR_MSG);
       return 0;
       }
   }
nextChar(&state->input,0);

/* Substitute in parameters.. */
rawVal = getStringBufferString(&buffer);
val = substitute(state,rawVal);
if(val == NULL)
   {
   parseErrorStoredPos(state,  "Error with parameter: %s",rawVal);
   free(rawVal);
   free(val);
   return 0;
   }
free(rawVal);

i = 0;
while( functions[functionIdx].validParams[i] )
   {
   if( strcmp(key,functions[functionIdx].validParams[i])==0)
      break;
   i++;
   }
if( !functions[functionIdx].validParams[i] )
   {
   parseErrorStoredPos(state,
		       "Invalid SSI parameter %s (discarded)",
		       key);
   free(key);
   free(val);
   return 1;
   }
 
/*  Register keyval pair */
if(!listAppendKey(&state->param,key,(long)val))
   {
   /* If this errors, its going to be due to a Malloc failure */
   parseError(state,MALLOC_ERROR_MSG);
   free(key);
   free(val);
   return 0;
   }
return 1;
}




/* Parses SSI. Assumes *state has been loaded with the necessary stuff
   (input, output, options, etc)
*/
void parseSSI(State_t *state)
{
const char *openTag = "<!--#";
const char *endTag  = "-->";
int c = 0; /* Only initialised in order to make GCC shut up */
int len;
int functionIdx = 666;

initStringBuffer(&state->condStk);
sbPush(&state->condStk,-1);
assert( state->condStk.length == 1);

while( nextChar(&state->input,1) != EOF )
   {
   storePos(&state->input);
   for(len=0;len<strlen(openTag);len++)
      {
      c = nextChar(&state->input,0);
      if( c == EOF || c != openTag[len])
	 break;
      if( len == 0 )
	 storeStartPos(&state->input);
      }
   if( len < strlen(openTag) )
      {
      /*  Non-SSI content (output neat) */
      int i;
      if( sbPeek(&state->condStk) <= 0)
	 {
	 for(i=0; i<len; i++)
	    outputChar(state->output,(char)openTag[i]);
	 if( c != EOF )
	    outputChar(state->output,c);
	 }
      }
   else /* SSI content */
      {
      int escape = 0;
      char function[MAX_FUNCTION_NAME_LEN+2];
      
      /* Get SSI function name */
      storePos(&state->input);
      len = 0;
      /* Use peek to make sure next char is alpahnumeric. Using pop screws
	 <!--#func--> up as the '-' after 'func' gets lost
	 if( isalnum(c) ) */
      while(1)
	 {
	 if( !isalnum( nextChar(&state->input,1) ) )
	    break;
	 if( len < MAX_FUNCTION_NAME_LEN )
	    {
	    function[len] = nextChar(&state->input,0);
	    len++;
	    }
	 }
      function[len] = 0;
      if( len == 0 )
	 {
	 parseError(state,"Missing SSI function name", c);
	 errorFindClose(state,endTag);
	 continue;
	 }

      /* Check SSI function name */
      functionIdx = 0;
      while( functions[functionIdx].name )
	 {
	 if( compareStrings(function,functions[functionIdx].name)==0 )
	    break;
	 functionIdx++;
	 }
      if( !functions[functionIdx].name )
	 {
	 parseErrorStoredPos(state,"%s is not a valid SSI function", function);
	 errorFindClose(state,endTag);
	 continue;
	 }

      /* Should be some seperating space
	 if( !isspace(c) )
	 {
	 parseError(state,"Expected whitespace, got '%c'", c);
	 errorFindClose(state,endTag);
	 } */

      parseEatSpace(state);

      /* Deal with key/value pairs */
      escape = 0;
      while( isalnum( nextChar(&state->input,1) ) )
	 {	    
	 if( ! parseKeyValue(state,functionIdx) )
	    {
	    escape = 1;
	    break;
	    }
	 parseEatSpace(state);
	 }
      if( escape )
	  {
	  errorFindClose(state,endTag);
	  continue;
	  }

      /* Check for end tag */
      if( nonerrorFindClose(state,endTag) )
	 {
	 /* Entire SSI directive parsed successfully. Now run it.. */
	 if( state->param.count != functions[functionIdx].numParams &&
	     functions[functionIdx].numParams >= 0 )
	     {
	     parseError(state,
			"%s takes %i parameters, not %i"
			" (misspelled parameters?)",
			functions[functionIdx].name,
			functions[functionIdx].numParams,
			state->param.count);
	     }
	 else if( state->param.count < -functions[functionIdx].numParams &&
	     functions[functionIdx].numParams < 0 )
	     {
	     parseError(state,
			"%s needs at least %i parameters, not %i"
			" (misspelled parameters?)",
			functions[functionIdx].name,
			-functions[functionIdx].numParams,
			state->param.count);
	     }
	 else if( functionIdx > 7 || sbPeek(&state->condStk) <= 0)

	    {
	    functions[functionIdx].func(state);
	    }
	 listReset(&state->param,1);
	 continue;
	 }
      } /* end SSI content */
   }
}

/* }}} */
