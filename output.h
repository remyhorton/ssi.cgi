/*
  output.h - Output buffering
  Provides a central point thru which all output is funelled. This primaraly 
  is to make it easy to send output to different places. It also allows for 
  output to be buffered, combining a lot of small system function calls into a 
  few bigger ones.

  Disabling output buffering saves 10kb (quite a lot for a 50-60kb binary),
  as practically this entire file (including the function calls) gets
  optimised away. However, this is likley to butcher performance (especially
  for FastCGI) as non-SSI content is output a character at a time.

  There are grounds (from a software engineering perspective) for sharing
  common code between this and the StringBuffer module, but since both 
  are extensivly inlined, it would likley get messy.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdarg.h>

/* Initialises an output. It is assumed memory has already been allocated
*/
static __inline__ void initOutput(Output_t *output)
{
#ifndef UNBUFFERED_OUTPUT
output->length = 0;
output->buffer[0] = 0;
assert(OUTPUT_BUFFER_CAPACITY > 1);
#endif
}


/* Cause all buffered output to be dispatched/printed.
*/
static __inline__ void flushOutput(Output_t *output)
{
#ifdef FASTCGI

#ifndef UNBUFFERED_OUTPUT
output->flushFunc(output->fp,output->buffer,output->length);
output->length = 0;
output->buffer[0] = 0;
#endif

#else /* FASTCGI */

#ifndef UNBUFFERED_OUTPUT
output->buffer[OUTPUT_BUFFER_CAPACITY] = 0;
fprintf(output->fp,"%s",output->buffer);
output->length = 0;
output->buffer[0] = 0;
#endif
fflush(output->fp);

#endif /* FASTCGI */
}


/* Copy a string to the output buffer
*/
static void __inline__ outputString(Output_t *output, const char *str)
{
#ifdef UNBUFFERED_OUTPUT

#ifdef FASTCGI
 output->flushFunc(output->fp,str, strlen(str));
#else
 fprintf(output->fp,"%s",str);
#endif

#else
int strLen = strlen(str);
int availLen = OUTPUT_BUFFER_CAPACITY - output->length;
const char *leftStr = str;
while( strLen > availLen )
   {
   strncpy(&output->buffer[output->length], leftStr, availLen);
   leftStr = &leftStr[availLen];
   strLen += -availLen; 
   flushOutput(output);
   availLen = OUTPUT_BUFFER_CAPACITY;  
   }
strncpy(&output->buffer[output->length], leftStr, strLen);
output->length += strLen;
if( output->length >= OUTPUT_BUFFER_CAPACITY )
  flushOutput(output);
#endif
}


/* Print to output buffer via a vprintf interface. You'll need an in-depth 
   knowledge of stdarg.h (variable numbers of function parameters) to 
   understand this function.
*/
static void __inline__ outputVfPrintf(Output_t *output, const char *str, 
				      va_list args)
{
#ifdef UNBUFFERED_OUTPUT

#ifdef FASTCGI
char buffer[512];
vsnprintf(buffer, 512, str, args);
output->flushFunc(output->fp,buffer, strlen(buffer));
#else
vfprintf(output->fp,str,args);
#endif

#else
char buffer[512];
vsnprintf(buffer, 512, str, args);
outputString(output, buffer);
#endif
}


/* Print to output buffer via a printf-like interface
*/
static void __inline__ outputPrintf(Output_t *output, char *str, ...)
{
va_list args;
va_start(args,str);
outputVfPrintf(output, str, args);
va_end(args);
}


/* Write a single character to the output buffer
*/
static void __inline__ outputChar(Output_t *output, const char letter)
{
#ifdef UNBUFFERED_OUTPUT

#ifdef FASTCGI
 char tmp[1] = {letter};
 output->flushFunc(output->fp,tmp, 1);
#else
 fprintf(output->fp,"%c",letter);
#endif

#else
if( output->length >= OUTPUT_BUFFER_CAPACITY )
   flushOutput(output);
output->buffer[output->length] = letter;
output->length++;
output->buffer[output->length] = 0;
if( output->length >= OUTPUT_BUFFER_CAPACITY )
   flushOutput(output);
#endif
}
