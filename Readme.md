SSI.cgi: Standalone SSI interpretor
===================================

SSI.cgi is a standalone SSI (Server Side Includes) interpreter, intended for use with lightweight webservers (such as Cherokee) that themselves do not support SSI. It is implemented using C, with the intention of minimizing both overheads and dependency requirements. 

## Background 
The project started when I was considering possible alternatives to [Apache](http://httpd.apache.org/), the webserver I have used in the past for both my own websites and those I have setup for other people. I came across [Cherokee](http://www.cherokee-project.com/), which seemed to offer a good balance between being lightweight, ease of setup, and features that are useful. In particular its ability to run scripts under different Unix users didn't require the awkward setting up that ``suexec`` and ``suPHP`` needed. The big problem is that my (then) more recent sites used SSI to avoid duplicating common page components such as headers. Although one possibility was to convert the sites to use PHP instead, this was not something I wanted to do. I also didn't want to use Perl, which was required by the SSI parsers I came across on the web. In the end I decided to write my own parser, and SSI.cgi was the result.


## Current status
Functionally SSI.cgi is no longer being developed, although it has had some code clean-ups, most notably the stripping out of a lot of dead code that was within comments. Aside from a quick fix to silence a compiler warning on modern 64-bit systems the program logic has not been changed for the last decade or so. The version number has been bumped up to 1.9 but that was due to the regeneration of the build configure scripts that still used my old university email address.


## Documentation

See [Documentation](./Documentation.md) (also [as PDF](./Documentation.pdf)).

## Licence
IconSharp is licenced under [version 2 of the GPL][gpl2].

## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

[gpl2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
