/*
  list.c - List handling internals
  Previously there were two simular key/value pair modules - one for string 
  values, and one for integers. This combined module replaces both of them. 
  Most of the inlining has also been removed, partly to make the code cleaner,
  and partly to reduce code size. 

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ssi.h"


/*****************************************************************************
 ** Internal memory allocation code
 *****************************************************************************/

static int listNewBlock(List_t *list)
{
/* {{{ */
int i;
char **newblock = (char**) 
   malloc( sizeof(char**) + (sizeof(ListLink_t)*LIST_BLOCK_SIZE) );
if(!newblock)
   return 0;
((char**)newblock)[0] = 0;
if(!list->memory)
   list->memory = newblock;
else
   list->memoryTail[0] = (char*)newblock;
list->memoryTail = newblock;
list->unused = (ListLink_t*)(&newblock[1]);

for(i=0;i<LIST_BLOCK_SIZE-1;i++)
   list->unused[i].next = &list->unused[i+1];
list->unused[i].next = 0;
return 1;
/* }}} */
}


/*****************************************************************************
 ** Init & Delete routines
 *****************************************************************************/

/* Init list
   Ideally this function should be inlined due to its size, but then 
   newlistNewBlock would have to become public
*/
int listInit(List_t *list)
{
assert( sizeof(char*)==sizeof(char**) );
assert( sizeof(char*)==sizeof(int) );
memset(list, 0, sizeof(List_t));
return listNewBlock(list);
}


void listDelete(List_t *victim, const int freeValues)
{
/* {{{ */
char **next;
char **current = victim->memory;
ListLink_t *link = victim->anchor;
while(link)
   {
   if(freeValues) 
      free((char*)link->value);
   free(link->key);
   link = link->next;
   }
while(current)
   {
   next = (char**)current[0];
   free(current);
   current = next;
   }
/* }}} */
}


void listReset(List_t *list, const int freeValues)
{
/* {{{ */
ListLink_t *current = list->anchor;
while(current)
   {
   if( freeValues )
      free((char*)current->value);
   free(current->key);
   current = current->next;
   }
if( list->anchor )
   {
   assert( list->tail != NULL );
   list->tail->next = list->unused;
   list->unused = list->anchor;
   list->anchor = NULL;
   list->tail   = NULL;
   }
list->count = 0;
list->nextint = 0;
/* }}} */
}


/*****************************************************************************
 ** Access functions
 *****************************************************************************/

/* Key getting function. Used internally
*/
ListLink_t *listFetch(List_t *list, char *key)
{
/* {{{ */
ListLink_t *current = list->anchor;
while(current)
    {
    if( compareStrings(current->key,key)==0 )
       return current;
    current = current->next;
    }
return NULL;
/* }}} */
}


/* Adds key/value pair to end of list. 
    Assumes *key is to come under ownership of the list  
*/
int listAppendKey(List_t *list, char *key, long value)
{
/* {{{ */
ListLink_t *newlink = list->unused;

if( newlink == NULL )
   {
   if( !listNewBlock(list) )
      return 0;
   newlink = list->unused;
   }

list->unused = list->unused->next;
newlink->key = key;
newlink->value = (char*)value;
newlink->next = NULL;

if(list->anchor)
   list->tail->next = newlink;
else
   list->anchor = newlink;
list->tail = newlink;
list->count++;
return 1;
/* }}} */
}


/* Adds key & integer value to end of list. 
    Assumes *key is to come under ownership of the list  
*/
int listFetchId(List_t *list, char *key)
{
/* {{{ */
ListLink_t *link = listFetch(list,key);
if( link )
   {
   /*link->value = value;*/
   free(key);
   return (long)link->value;
   }
else if( listAppendKey(list, key, list->nextint) )
   {
   list->nextint++;
   return list->nextint-1;
   }
return 0;
/* }}} */
}


/* Adds key & string value to end of list. 
    Assumes *key and *value is to come under ownership of the list  
*/
int listAddStr(List_t *list, char *key, char *value)
{
/* {{{ */
ListLink_t *link = listFetch(list,key);
if( link )
   {
   free((char*)link->value);
   link->value = value;
   //FIXME: Check for key==null
   free(key);
   return 1;
   }
else
   return listAppendKey(list, key, (long)value);
/* }}} */
}
