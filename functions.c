/*
  functions.c - SSI functions
  These cannot be inlined as they are accessed by a function pointer table.
  This is rather convenient, as it also allows them to be hived off to a 
  seperate compile unit (mercifully - the amount of inlining makes ssi.c
  touch and go when it comes to managability.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include "ssi.h"
#include <time.h> /* gmtime() & struct time_t */
#include <errno.h>

void parseSSI(State_t *state);


/* Most of this is copied out of parseErrorBody, which is local to ssi.c
*/
static void runError(State_t *state, char *str, ...)
{
/* {{{ */
va_list args;
va_start(args,str);
#ifndef NO_LINE_NUMBERS
if( !state->options->noLogErrors )
   {
   outputPrintf(state->errors,"SSI.exe Error (%i:%i): ",
		state->input.posStart.line,
		state->input.posStart.col);
   outputVfPrintf(state->errors,str,args);
   outputChar(state->errors,'\n');
   }
if( state->options->logErrorsToUser )
   {
   outputPrintf(state->output,"[SSI.exe Error (%i:%i): ",
		state->input.posStart.line,
		state->input.posStart.col);
   outputVfPrintf(state->output,str,args);
   outputChar(state->output,']');
   }
#else
if( !state->options->noLogErrors )
   {
   outputPrintf(state->errors,"SSI.exe Error: ");
   outputVfPrintf(state->errors,str,args);
   outputChar(state->errors,'\n');
   }
if( state->options->logErrorsToUser )
   {
   outputPrintf(state->output,"[SSI.exe Error: ");
   outputVfPrintf(state->output,str,args);
   outputChar(state->output,']');
   }
#endif
else
   outputString(state->output,state->options->errorMsg);
va_end(args);
/* }}} */
}

void funcNOP(State_t *state)
{
}


/*****************************************************************************
 ** Conditional expression parsing
 *****************************************************************************/
/* {{{ */

#define SUBS_OP_LESSEQ  1
#define SUBS_OP_LESS    2
#define SUBS_OP_MOREEQ  3
#define SUBS_OP_MORE    4
#define SUBS_OP_EQ      5
#define SUBS_OP_NOTEQ   6

static int ecString(State_t *state, char **cond, List_t *vars)
{
/* {{{ */
int quoted = 0;
StringBuffer_t sb;
char *str;
if( !initStringBuffer(&sb) )
   return -1;
while( **cond )
   {
   if( isalnum(**cond) )
      appendStringBuffer(&sb, **cond );
   else if( **cond == '\'' )
      quoted = !quoted;
   else if( quoted )
      appendStringBuffer(&sb, **cond );      
   else
      break;
   (*cond)++;
   }
if( quoted )
   return -1;
if( (str = getStringBufferString(&sb)) == NULL )
   return -1;
return listFetchId(vars,str);
/* }}} */
}


static __inline__ char *ecRegExp(State_t *state, char **cond)
{
/* {{{ */
char *start = ++(*cond);
while( **cond && isalnum(**cond) )
   (*cond)++;
if( **cond != '/' )
   return NULL;
**cond = 0;
(*cond)++;
return start;
/* }}} */
}

static __inline__ void ecEatWhitespace(State_t *state, char **cond,List_t *vars)
{
while( (**cond) && isspace(**cond) )
   (*cond)++;
}

static __inline__ int ecGetOperator(State_t *state, char **cond, List_t *vars)
{
/* {{{ */

switch( **cond )
   {
   case '<':
      (*cond)++;
      if( **cond == '=' )
	 {
	 (*cond)++;
	 return SUBS_OP_LESSEQ;
	 }
      return SUBS_OP_LESS;
   case '>':
      (*cond)++;
      if( **cond == '=' )
	 {
	 (*cond)++;
	 return SUBS_OP_MOREEQ;
	 }
      return SUBS_OP_MORE;
   case '=':
      (*cond)++;
      if( **cond == '=' )
	 (*cond)++;
      return SUBS_OP_EQ;
   case '!':
      (*cond)++;
      if( **cond == '=' )
	 {
	 (*cond)++;
	 return SUBS_OP_NOTEQ;
	 }
      runError(state,"Bad condition: Expected [<>!=], got '%c'",**cond);
      return -1;
   }
return -1;

/* }}} */
}

static __inline__ int ecBoolean(State_t *state, int var1, int operator, int var2)
{
/* {{{ */
switch(operator)
   {
   case SUBS_OP_LESSEQ:
      return (var1 <= var2);
   case SUBS_OP_LESS:
      return (var1 <  var2);
   case SUBS_OP_MOREEQ:
      return (var1 >= var2);
   case SUBS_OP_MORE:
      return (var1 >  var2);
   case SUBS_OP_EQ:
      return (var1 == var2);
   case SUBS_OP_NOTEQ:
      return (var1 != var2);
   default:
      runError(state,"Unknown comparison op %i. Internal Error.",operator);
      return -1;
   }

/* }}} */
}

static __inline__ int ecCondition(State_t *state, char **cond, List_t *vars)
{
/* {{{ */
int negate = 0;
int var1;
int var2;
int operator;
int result;
int result2;
/*
  char *var1Str;
  int var1Len;
*/

ecEatWhitespace(state,cond,vars);
if(**cond=='!')
   {
   negate = 1;
   (*cond)++;
   ecEatWhitespace(state,cond,vars);
   }
if( isalnum(**cond) || **cond == '\'')
   {
   /* {{{ Next token is string */
/*   var1Str = *cond;
     var1Len = (int)(*cond - var1Str);
*/
   var1 = ecString(state,cond,vars);   
   ecEatWhitespace(state,cond,vars);
   switch( **cond )
      {
      case 0:
	 return negate ? (!var1) : var1;
      case '<':
      case '>':
      case '=':
      case '!':
	 operator = ecGetOperator(state,cond,vars);
	 ecEatWhitespace(state,cond,vars);
	 if( **cond == '/' && 0)
	    {
	    /*
	      //char *regExp = ecRegExp(state,cond);
	      //	    var1Str[var1Len] = 0;
	      //http://www.pcre.org/
	      //FIXME: Do regexp
	      
	      //printf(">>>%s %s\n",var1Str,regExp);
	      //exit(1);
	    */
	    runError(state,"RegExp in conditions not supported");
	    return -1;
	    }
	 else
	    {
	    var2 = ecString(state,cond,vars);
	    result = ecBoolean(state,var1,operator,var2);
	    }
	 break;
      case ')':
	 return negate ? (!var1) : var1;
      default:
	 runError(state,"Bad condition: Expected [<>!=)], got '%c'",**cond);
	 return -1;
      }
   /* }}} */
   }  

else if( **cond == '(') 
   {
   (*cond)++;
   result = ecCondition(state,cond,vars);
   ecEatWhitespace(state,cond,vars);
   if( **cond != ')' )
      {

      runError(state,"Bad cssondition: Expected ')', got '%c' [%s]",
	       **cond,*cond);
      return -1;
      }
   (*cond)++;
   }
else
   {
   runError(state,"Bad condition: Expected [<>!=)], got '%c'",**cond);
   return -1;
   }

if(negate)
   result = !result;

/* {{{ If boolean op next, process it */
ecEatWhitespace(state,cond,vars);
switch( **cond )
   {
   case '&':
      (*cond)++;
      if(**cond=='&')
	 {
	 (*cond)++;
	 result2 = ecCondition(state,cond,vars);
	 if( result==-1 || result2==-1)
	    return -1;
	 return (result && result2);
	 }
      else
	 {
	 runError(state,"Bad condition: Expected '&', got '%c'",**cond);
	 return -1;
	 }
   case '|':
      (*cond)++;
      if(**cond=='|')
	 {
	 (*cond)++;
	 result2 = ecCondition(state,cond,vars);
	 if( result==-1 || result2==-1)
	    return -1;
	 return (result || result2);
	 }
      else
	 {
	 runError(state,"Bad condition: Expected '|', got '%c'",**cond);
	 return -1;
	 }
   }
/* }}} */

return result;
/* }}} */
}

static __inline__ int evalConditionString(State_t *state, char *condition)
{
/* {{{ */
List_t vars;
int result;
listInit(&vars);
result = ecCondition(state,&condition,&vars);
listDelete(&vars,0);
return result;
/* }}} */
}

/* }}} */


/*****************************************************************************
 ** Directive callbacks
 *****************************************************************************/

/* Compare strings by only checking the first character
   Parameter names are checked for validity before these execution callbacks 
   are run, so in most cases only the first letter needs to be looked at to 
   decide what to do. Judging by the large saving, GCC does a good job of
   stripping out stuff like the unused part of the constant string in the
   calling function, and then doing optimisation on the caller. This is nice, 
   as having a drop-in replacment for the full string check keeps the clarity 
   of what is expected by the code.
*/
static __inline__ int lazyCheck(const char *str1, const char *str2)
{
/* return compareStrings(str1, str2);*/
return ( *str1 != *str2 );
}


void funcIf(State_t *state)
{
/* {{{ */
int result = !evalConditionString(state,state->param.anchor->value);
if( state->condStk.length > 1 && sbPeek(&state->condStk) > 0)
   result = 2;
sbPush(&state->condStk,result);
/* }}} */
}

void funcElseIf(State_t *state)
{
/* {{{ */
int result = !evalConditionString(state,state->param.anchor->value);
if( state->condStk.length <= 1 )
   runError(state,"Unpaired #elif statement");
else if( sbPop(&state->condStk) == 1 )
   sbPush(&state->condStk,result);
else
   sbPush(&state->condStk,2);
/* }}} */
}

void funcElse(State_t *state)
{
/* {{{ */
if( state->condStk.length <= 1 )
   runError(state,"Unpaired #else statement");
else if( sbPop(&state->condStk) == 1 )
   sbPush(&state->condStk,0);
else
   sbPush(&state->condStk,2);
/* }}} */
}

void funcEndIf(State_t *state)
{
/* {{{ */
if( state->condStk.length <= 1 )
   runError(state,"Unpaired #endif statement");
else
   sbPop(&state->condStk);
/* }}} */
}


size_t virtualWriteCallback(void *ptr, size_t size, size_t count, 
			    void *state)
{
/* {{{ */
int i;
char *in = (char*)ptr;
char buffer[128];
int length = 0;

assert( size == sizeof(char) );
for(i=0;i<count;i++)
    {
    buffer[length] = in[i];
    length++;
    if( length == 127 )
	{
	buffer[length] = 0;
	outputString(((State_t*)state)->output,buffer);
	length = 0;
	}    
    }
buffer[length] = 0;
outputString(((State_t*)state)->output,buffer);

return size*count;
/* }}} */
}


/* Checks for path either statin with ../ or containing /../   
*/
static __inline__ int includePathCheck(State_t *state, const char *path)
{
/* {{{ */
int pos = 0;
int length = strlen(path);
if( !state->options->allowDotDot )
   {
   if(length > 3 && path[pos]=='.' && path[pos+1]=='.' && path[pos+2]=='/')
      return 0;
   while( pos-4 < length )
      {
      if( path[pos] == '/' && path[pos+1] == '.' && 
	  path[pos+2] == '.' && path[pos+3] == '/')
	 return 0;
      pos++;
      }
   }
return 1;
/* }}} */
}

void funcInclude(State_t *state)
{
/* {{{ */
char *fileName = state->param.anchor->value;

if( lazyCheck("file",   state->param.anchor->key)==0 ||
    lazyCheck("docroot",state->param.anchor->key)==0 ||
    lazyCheck("virtual",state->param.anchor->key)==0)
    {    
    /* {{{ */
    char *fullpath = NULL;
    char firstLetter = state->param.anchor->key[0];
    if( firstLetter == 'f' || firstLetter == 'F' )
       fullpath = appendStr(state->options->basePath,fileName);    
    else  
       {
       ListLink_t *docRoot=listFetch(&state->options->envvars,"DOCUMENT_ROOT");
       if(docRoot != NULL )
	  fullpath = appendStr(docRoot->value,fileName);  
       }
    if( fullpath == NULL )
	runError(state,MALLOC_ERROR_MSG);
    else if( !includePathCheck(state, fullpath) )
       { /*  ../ detected, and they are not permitted */
       runError(state,
		"'..' is disallowed in #include (using %s=)",
		state->param.anchor->key);
       free(fullpath);
       }
    else
	{
	/*FIXME: Only parse files with specific extensions? */
	int isSSI = state->options->parseIncludeFile;
	
	if( isSSI == 2)
	    {
	    int offset = strlen(fileName) - 1;
	    if( offset <= 0 || fileName[offset] == '.' )
		isSSI = 0;
	    else
		{
		while( offset >= 0 )
		    {
		    if( fileName[offset] == '.' )
			break;
		    offset--;
		    }
		if( listFetch(&state->options->SSIextensions,
			      &fileName[offset+1]) != NULL 
		    )
		    isSSI = 1;
		else
		    isSSI = 0;
		}
	    } 

	if( isSSI )
	    { 
	    /* {{{  Parse inclued file for SSI tags */

	    State_t morestate;
	    if( !initInputFile(&morestate.input, fullpath) )
		runError(state,"Unable to parse '%s'",fullpath);	    
	    else
		{
		listInit(&morestate.param);
		morestate.output = state->output;
		morestate.errors = state->errors;
		morestate.options = state->options;
		parseSSI(&morestate);
		listDelete(&morestate.param,1);
		}
	    /* }}} */
	    }
	else
	    { 
	    /* {{{ Simply echo the file	*/
	    FILE *fp = fopen(fullpath,"r");
	    if( fp != NULL )
		{
		char buffer[512];
		int read;
		while( 1 )
		    {
		    read = fread(buffer, sizeof(char), 512, fp);
		    if( read < 1 )
			break;	
		    if( fwrite(buffer, sizeof(char), read, stdout) < read )
			{
			runError(state,"Error echoing '%s'",fullpath);
			break;
			}
		    }		
		fclose(fp);	      
		}
	    else
		runError(state,"Unable to open %s",state->param.anchor->value);
	    /* }}} */
	    }
	free(fullpath);
	}

    /* }}} */
    }

/* }}} */
}


char **cmdToArgvUNUSED(char *cmd)
{
/* {{{ */

char **argv = (char**)malloc(sizeof(char**)*4);
char **newargv;
int limit = 4;
int count = 0;
/*
  //int length = strlen(cmd);
  //int offset = 0;
*/

assert(*cmd);
if(!argv)
   return NULL;
while( *cmd )
   {
   while( *cmd && isspace(*cmd) )
      cmd++;
   if( !(*cmd) )
      break;
   if( count==limit )
      {
      limit = limit<<1;
      newargv = (char**)realloc(argv,sizeof(char**)*limit);
      if(!newargv)
	 {
	 free(argv);
	 return NULL;
	 }
      argv = newargv;
      }
   argv[count] = cmd;
   count++;
   while( *cmd && !isspace(*cmd) )
      cmd++;
   if(*cmd == 0)
      break;
   *cmd = 0;
   cmd++;
   }
return argv;

/* }}} */
}


void funcExec(State_t *state)
{
/* {{{ */
if( lazyCheck("cgi",state->param.anchor->key)==0 )
   {    
   /* FIXME: Implement exec cgi (non-virtual) */
   runError(state,"SSI #exec cgi is not supported. Use exec cmd.");
   /* Apache recommends include virtual, but since its disabled.. */
   }
else /* has to be cmd */
   {
   /* {{{ */

#ifndef ENABLE_EXEC_CMD
    runError(state,"SSI #exec cmd is disabled.");
#else
    char **argv = cmdToArgv(state->param.anchor->value);
    int oldHandles[3];
    int stdOut[2];
    int stdErr[2];
    if(!argv)
       runError(state,MALLOC_ERROR_MSG);
    else
       {
       pipe(stdOut);
       pipe(stdErr);
       oldHandles[1] = dup(1); /*out*/
       oldHandles[2] = dup(2); /*err*/
       close(1);
       close(2);
       dup2(stdOut[1], 1);
       dup2(stdErr[1], 2);
       if( !fork() )
	  {
	  /* {{{ child */
	  close( stdOut[0]);
	  close( stdOut[1]);
	  close( stdErr[0]);
	  close( stdErr[1]);
	  execvp(argv[0],argv);
	  fprintf(stderr,"ERROR: fork() failed (%s)", strerror(errno) );
	  exit(1);
	  /* }}} */
	  }
       else
	  {
	  /* {{{ parent */
	  char input[1024];
	  int result;
	  close(1);
	  dup2(oldHandles[1],1);
	  dup2(oldHandles[2],2);
	  close(stdOut[1]);
	  close(stdErr[1]);
	  printf("$$$\n");fflush(stdout);
	  while(1) 
	     {
	     /* {{{ Mirror stdout to client */
	     result = read(stdOut[0],input,1023);
	     if( result > 0 )
		{
		input[result] = 0;
		outputString(state->output,input);
		if( result < 1023 )
		   break;
		}
	     else if( result == 0 )
		break;
	     else
		{
		runError(state,"exec cmd: Error reading stdout");
		break;
		}
	     /* }}} */
	     }	  
	  while(1) 
	     {
	     /* {{{ Mirror stderr to client */
	     result = read(stdErr[0],input,1023);
	     /*FIXME: Suppress stderr*/
	     if( result > 0 )
		{
		input[result] = 0;
		result--;
		while( result > 0 )
		   if( input[result] == '\r' || input[result] == '\n')
		      input[result] = 0;
		runError(state,"exec cmd stderr: %s",input);
		if( result < 1023 )
		   break;
		}
	     else if( result == 0 )
		break;
	     else
		{
		runError(state,"exec cmd: Error reading stderr");
		break;
		}
	     /* }}} */
	     }
	  close(stdOut[0]);
	  close(stdErr[0]);
	  /* }}} */
	  }
       }
#endif /* exec cmd enabled */
    /* }}} */
    }

/* }}} */
}


static __inline__ void printUrlEncodedString(const char *str, Output_t *output)
{
/* {{{ */
const unsigned char *current = (unsigned char *) str;
while( *current )
   {
   if( *current < 31 || *current > 127 )
       outputPrintf(output,"%%-%x",*current);
   else
      switch(*current)
	 {
	 case '$':  case '&':
	 case '+':  case ',':
	 case '/':  case ':':
	 case ';':  case '=':
	 case '?':  case '@':
	    outputPrintf(output,"%%%02x",*str);
	    break;
	 default:
	    outputChar(output,*current);
	    break;	 
	 }
   current++;
   }
/* }}} */
}


static __inline__ void printEntityEncodedString(const char *str, Output_t *output)
{
/* {{{ */
const char *current = str;
static const unsigned char entityEncoding[256][9] = 
 {
    /* {{{ */
  {0,0}, {1,0}, {2,0}, {3,0}, {4,0}, {5,0}, {6,0}, {7,0}, {8,0}, {9,0}, 
  {10,0},{11,0},{12,0},{13,0},{14,0},{15,0},{16,0},{17,0},{18,0},{19,0},
  {20,0},{21,0},{22,0},{23,0},{24,0},{25,0},{26,0},{27,0},{28,0},{29,0},
  {30,0},{31,0},{32,0},{33,0},"&quot;",{35,0},{36,0},{37,0},"&amb;",{39,0},
  {40,0},{41,0},{42,0},{43,0},{44,0},{45,0},{46,0},{47,0},{48,0},{49,0},
  {50,0},{51,0},{52,0},{53,0},{54,0},{55,0},{56,0},{57,0},{58,0},{59,0},
  "&lt;",{61,0},"&gt;",{63,0},{64,0},{65,0},{66,0},{67,0},{68,0},{69,0},
  {70,0},{71,0},{72,0},{73,0},{74,0},{75,0},{76,0},{77,0},{78,0},{79,0},
  {80,0},{81,0},{82,0},{83,0},{84,0},{85,0},{86,0},{87,0},{88,0},{89,0},
  {90,0},{91,0},{92,0},{93,0},{94,0},{95,0},{96,0},{97,0},{98,0},{99,0},
  {100,0},{101,0},{102,0},{103,0},{104,0},{105,0},{106,0},{107,0},{108,0},
  {109,0},
  {110,0},{111,0},{112,0},{113,0},{114,0},{115,0},{116,0},{117,0},{118,0},
  {119,0},
  {120,0},{121,0},{122,0},{123,0},{124,0},{125,0},{126,0},{127,0},{128,0},
  {129,0},
  {130,0},{131,0},{132,0},{133,0},{134,0},{135,0},{136,0},{137,0},{138,0},
  {139,0},
  {140,0},{141,0},{142,0},{143,0},{144,0},{145,0},{146,0},{147,0},{148,0},
  {149,0},
  {150,0},{151,0},{152,0},{153,0},{154,0},{155,0},{156,0},{157,0},{158,0},
  {159,0},
  "&nbsp;",  "&acute;", "&micro;",  "&para;",  "&middot;",
  "&cedil;", "&sup1;",  "&ordm;",   "&raquo;", "&frac14;",
  "&frac12;","&fraq34;","&iquest;", "&agrave;","&aacute;",
  "&acirc;", "&atilde;","&auml;",   "&aring;", "&aelig;", 
  "&ccdil;", "&egrave;","&eacute;", "&ecirc;", "&euml;",  
  "&igrave;","&iacute;","&icirc;",  "&iuml;",  "&eth;",   
  "&ntilde;","&ograve;","&oacute;", "&ocirc;", "&otilde;",
  "&ouml;",  "&times;", "&oslash;", "&ugrave;","&uacute;",
  "&ucirc;", "&uuml;",  "&yacute;", "&thorn;", "&szlig;", 
  "&agrave;","&aacute;","&acirc;",  "&atilde;","&auml;",  
  "&aring;", "&aelig;", "&ccedil;", "&egrave;","&eacute;",
  "&ecirc;", "&euml;",  "&igrave;", "&acute;", "&icirc;", 
  "&iuml;",  "&eth;",   "&ntilde;", "&ograve;","&oacute;",
  "&ocirc;", "&tilde;", "&ouml;",   "&divide;","&oslash;",
  "&ugrave;","&uacute;","&ucirc;",  "&uuml;",  "&yacute;",
  "&thorn;", "&yuml;"
  /* }}} */
 };
while( *current )
   {
   outputString(output,(char*)entityEncoding[ (int)*current ]);
   current++;
   }
/* }}} */
}


#define ECHO_ENCODING_NONE   0
#define ECHO_ENCODING_ENTITY 1
#define ECHO_ENCODING_URL    2

static void __inline__ 
printUsingEncoding(Output_t *output, const char *str, int encoding)
{
/* {{{ */
switch(encoding)
   {
   case ECHO_ENCODING_NONE:
      outputString(output,str);
      break;
   case ECHO_ENCODING_ENTITY:
      printEntityEncodedString(str,output);
      break;
   case ECHO_ENCODING_URL:
      printUrlEncodedString(str,output);
      break;
   }
/* }}} */
}


void funcEcho(State_t *state)
{
/* {{{ */

ListLink_t *current = state->param.anchor;
char *key;
char *value;
int encoding = ECHO_ENCODING_ENTITY;
int varsPresent = 0;

/*
  //FIXME: This function supposed to have special cases?
  //FIXME: Put into env vars instead..
*/

while(current)
    {
    key = current->key;
    value = current->value;
    if( lazyCheck("encoding",key)==0 )
	{
	/* {{{ */
	if( compareStrings("none",value) == 0 )
	   encoding = ECHO_ENCODING_NONE;
	else if( compareStrings("url",value) == 0 )
	   encoding = ECHO_ENCODING_URL;
	/* %-encoding for use in URLs */
	else if( compareStrings("entity",value) == 0 )
	   encoding = ECHO_ENCODING_ENTITY;
	/* HTML encoding (escape &, <, etc) - default */
	else
	   runError(state,"Unknown encoding: %s",value);
	/* }}} */
	}
    else if( lazyCheck("var",key)==0 )
	{
	ListLink_t *entry = listFetch(&state->options->uservars,value);
	varsPresent = 1;
	if( entry )
	   printUsingEncoding(state->output,entry->value,encoding);
#ifdef ENABLE_ENV_VARS
	else if( (entry = listFetch(&state->options->envvars,value)) )
	   printUsingEncoding(state->output,entry->value,encoding);
#endif
	/* {{{ SSI special variables */
	else if( compareStrings("DATE_GMT",value)==0)
	    {
	    /* {{{ */
	    char datestring[64];
	    time_t currtime;
	    struct tm *currTime;
	    time(&currtime);
	    currTime = gmtime(&currtime);
	    strftime(datestring, 64, state->options->timeFmt,currTime);
	    printUsingEncoding(state->output,datestring,encoding);
	    /* }}} */
	    }
	else if( compareStrings("DATE_LOCAL",value)==0)
	    {
	    /* {{{ */
	    char datestring[64];
	    time_t currtime;
	    struct tm *currTime;
	    time(&currtime);
	    currTime = localtime(&currtime);
	    strftime(datestring, 64, state->options->timeFmt,currTime);
	    printUsingEncoding(state->output,datestring,encoding);
	    /* }}} */
	    }
	else if( compareStrings("DOCUMENT_NAME",value)==0)
	   printUsingEncoding(state->output,state->options->fileName,encoding);
	else if( compareStrings("DOCUMENT_URI",value)==0)
	   printUsingEncoding(state->output,state->options->baseURI,encoding);
	else if( compareStrings("LAST_MODIFIED",value)==0)
	    {
	    /* {{{ */

	    char *fullpath = appendStr(state->options->basePath,
				       state->options->fileName);
	    if( fullpath == NULL )
		runError(state,"Memory error");
	    else
		{	    
		char datestring[64];
		struct stat fileinfo;
		if( stat(fullpath,&fileinfo) == -1 )
		    runError(state,"Unable to get datestamp");
		else
		    {
		    struct tm *time = gmtime(&fileinfo.st_mtime);
		    strftime(datestring, 64, state->options->timeFmt,time);
		    printUsingEncoding(state->output,datestring,encoding);
		    }
		free(fullpath);
		}

	    /* }}} */
	    }
	else if( compareStrings("QUERY_STRING_UNESCAPED",value)==0)
	    {
	    /* {{{ */
//	    char xxx[8] = "xxx%2f4";
	    ListLink_t *entry;
	    entry = listFetch(&state->options->envvars,"QUERY_STRING");

	    if( entry == NULL )
	       runError(state,"Unable to get query string");
	    else
	       {
	       /* {{{ Display in appropriate form */
	       char *current = entry->value;
	       while(*current)
		  {
		  char letter = *current;
		  /* Decode %xx escapes
		     if( letter == '%' && strlen(current) >= 3 && 
		     isxdigit(*(current+1)) && isxdigit(*(current+2)) )
		     { current++; current++;  letter = '$'; } */
		  switch( letter )
		     {
		     case '&':   case '|':   case '>':   case '<':
		     case '[':   case ']':   case '{':   case '}':
		     case ';':   case '\\':  case '#':   case '"':
		     case '\'':  case '`':   case '*':   case '?':
		     case '(':   case ')':   case '$':   case '=':
		     case ' ':   case '\t':  case '\n':  case '^':
			outputChar(state->output,'\\');
			/* fall thru */
		     default:
			outputChar(state->output,letter);
			break;
		     }
		  current++;
		  }
	       /* }}} */ 
	       }
	    /* }}} */
	    }
	/* }}} */ /* end of SSP special variables */

	else
	   outputString(state->output,state->options->echoMsg);
	}
    current = current->next;
    }
if( !varsPresent )
   runError(state,"Echo statement without var parameter");
/* }}} */
}


void funcConfig(State_t *state)
{
/* {{{ */
char *copy;

if( lazyCheck("sizefmt",state->param.anchor->key)==0 )
    {
    if( compareStrings("bytes",state->param.anchor->value)==0 )
	state->options->fSizeFmt = FSIZEFMT_BYTES;
    else if( compareStrings("abbrev",state->param.anchor->value)==0 )
	state->options->fSizeFmt = FSIZEFMT_ABBREV;
    else 
	runError(state,"Unknown file size format '%s'", 
		 state->param.anchor->value);
    }
else if( (copy=copyStr(state->param.anchor->value)) == NULL )
    runError(state,"Memory error");
else
    {
    if( lazyCheck("timefmt",state->param.anchor->key)==0 )
	{
	free(state->options->timeFmt);
	state->options->timeFmt = copy;
	}
    else if( lazyCheck("errmsg",state->param.anchor->key)==0 )
	{
	free(state->options->errorMsg);
	state->options->errorMsg = copy;
	}
    else if( lazyCheck("echomsg",state->param.anchor->key)==0 )
	{
	free(state->options->echoMsg);
	state->options->echoMsg = copy;
	}
    }

/* }}} */
}


void funcFiledate(State_t *state)
{
/* {{{ */

if( lazyCheck("file",state->param.anchor->key)==0 )
    {
    char *fullpath = 
	appendStr(state->options->basePath,state->param.anchor->value);
    if( fullpath == NULL )
	runError(state,"Memory error");
    else
	{
	char datestring[64];
	struct stat fileinfo;
	if( stat(fullpath,&fileinfo) == -1 )
	    runError(state,"Unable to get datestamp of file %s",
		     state->param.anchor->value);
	else
	    {
	    struct tm *time = gmtime(&fileinfo.st_mtime);
	    strftime(datestring, 64, state->options->timeFmt,time);
	    outputString(state->output,datestring);
	    }
	free(fullpath);
	}
    }
else if( lazyCheck("virtual",state->param.anchor->key)==0 )
    {
    /*FIXME: flastmod virtual support */
    runError(state,"SSI flastmod virtual support not enabled");
    }

/* }}} */
}


void funcFilesize(State_t *state)
{
/* {{{ */
const char *fileName = state->param.anchor->value;
char *fullpath = NULL;
char firstLetter = state->param.anchor->key[0];
if( firstLetter == 'f' || firstLetter == 'F' )
    fullpath = appendStr(state->options->basePath,fileName);    
else  
    {
    ListLink_t *docRoot = listFetch(&state->options->envvars,"DOCUMENT_ROOT");
    if(docRoot != NULL )
	fullpath = appendStr(docRoot->value,fileName);  
    }

if( fullpath == NULL )
    runError(state,"Memory error");
else
    {
    struct stat fileinfo;
    if( stat(fullpath,&fileinfo) == -1 )
	runError(state,"Unable to get size of file %s",
		 state->param.anchor->value);
    else
	{
	if( state->options->fSizeFmt == 0)
	    outputPrintf(state->output,"%i", (int)fileinfo.st_size);
	else
	    {
	    int size = fileinfo.st_size;
	    if( size < 1024 )
		outputPrintf(state->output,"%i",size);
	    else if( size < 1024*1024 )
		outputPrintf(state->output,"%iK",size>>10);
	    else if( size < 1024*1024*1024 )
		outputPrintf(state->output,"%iM",size>>20);
	    else 
		outputPrintf(state->output,"%iG",size>>30);
	    }
	}
    free(fullpath);
    }

/* }}} */
}


void funcPrintenv(State_t *state)
{
/* {{{ */
ListLink_t *current;
/* FIXME: Allow encoding used to be changed?*/
#ifdef ENABLE_ENV_VARS
current = state->options->envvars.anchor;
while(current != NULL )
    {
    printUrlEncodedString(current->key,state->output);
    outputString(state->output,"=");
    printUrlEncodedString(current->value,state->output); 
    outputString(state->output," \n");
    current = current->next;
    }
#endif
current = state->options->uservars.anchor;
while(current != NULL )
    {
    printUrlEncodedString(current->key,state->output);
    outputString(state->output,"=");
    printUrlEncodedString(current->value,state->output); 
    outputString(state->output," \n");
    current = current->next;
    }
/* }}} */
}


void funcSet(State_t *state)
{
/* {{{ */
char *var = NULL;
char *val = NULL;
ListLink_t *current = state->param.anchor;

while(current)
    {
    if( compareStrings("var",current->key)==0 )
	{
	if(!var)
	    var = copyStr(current->value);
	else
	    {
	    runError(state,"SET specifies var multiple times");
	    if(!val) free(val);
	    return;
	    }  
	}
    else if( compareStrings("value",current->key)==0 )
	{
	if(!val)
	    val = copyStr(current->value);
	else
	    {
	    runError(state,"SET specifies var multiple times");
	    if(!var) free(var);
	    return;
	    }  
	}
    current = current->next;
    }

if( !var || !val )
    {
    if( var  ) 	free(var);
    if( val  ) 	free(val);
    runError(state,MALLOC_ERROR_MSG);
    return;
    }

listAddStr(&state->options->uservars,var,val);
/* }}} */
}

