/*
  ssi.h - Main header file 
*/


#ifdef HAVE_CONFIG_H
 #include "config.h"
#else
 #define ENABLE_ENV_VARS
#endif

#define DEFAULT_ECHO_MESSAGE  "undefined"
#define DEFAULT_ERROR_MESSAGE "There was an error processing this directive"
#define DEFAULT_TIME_FORMAT   "%d/%m/%Y %H:%M:%S"
#define MALLOC_ERROR_MSG      "Memory allocation error"
#define FSIZEFMT_ABBREV        1
#define FSIZEFMT_BYTES         0
#define INPUT_BUFFER_SIZE      5
#define LIST_BLOCK_SIZE        1
#define OUTPUT_BUFFER_CAPACITY 1024
#define MAX_FUNCTION_NAME_LEN    30
#define MAX_FUNCTION_PARAM_COUNT 10


/****************************************************************************
 ** Variable storage (a linked list basically)
 ****************************************************************************/

/* {{{ Key/Value pair structures */
typedef struct ListLink_s ListLink_t;
struct ListLink_s
  {
  char blockBase;
  char *key;
  char *value;
  struct ListLink_s *next;
};

typedef struct 
  {
  char **memory;
  char **memoryTail;
  ListLink_t *anchor;
  ListLink_t *tail;
  ListLink_t *unused;
  int count;
  int nextint;
} List_t;
/* }}} */

/* {{{ Functions */
int  listInit  (List_t *list);
void listDelete(List_t *victim, const int freeValues);
void listReset (List_t *list, const int freeValues);
int  listFetchId(List_t *list, char *key);
int  listAddStr(List_t *list, char *key, char *value);
ListLink_t *listFetch(List_t *list, char *key);
static __inline__ int listGetCount(List_t *list)
{  return list->count;
}
int listAppendKey(List_t *list, char *key, long value);
/* }}} */


/****************************************************************************
 ** Program structures. 
 ****************************************************************************/
#include "stringbuffer.h"

/* {{{ Input buffering */

typedef struct
  {
  FILE *fp;
  char buffer[INPUT_BUFFER_SIZE+1];
  int offset;
  int length;
#ifndef NO_LINE_NUMBERS
  struct 
    {
    int line;
    int col;
    int isNewline;
  } posStored,posLast,posStart;
#endif
} Input_t;

/* }}} */

/* {{{ Output buffering */

typedef struct
  {
  void (*flushFunc)(FILE*,const char*,int);
  FILE *fp;
#ifndef UNBUFFERED_OUTPUT
  char buffer[OUTPUT_BUFFER_CAPACITY+1];
  int length;
#endif
} Output_t;

/* }}} */

/* {{{ SSI state structures */

typedef struct
  {
  List_t uservars;
  List_t envvars;
  List_t SSIextensions;
  char *errorMsg;
  char *echoMsg;
  char *timeFmt;
  unsigned char fSizeFmt;
  unsigned char noLogErrors;
  unsigned char logErrorsToUser;
  unsigned char parseIncludeFile;
  unsigned char noWarnings;
  unsigned char allowDotDot;
  unsigned char oneThread;

  char *fileName;
  char *basePath;    /* used for filesystem fetches */
  char *baseURI;     /* used for relative virtual fetches (and DOCUMENT_URI)*/
  int offsetURIpath; /* Offset to path in *baseURI */
} Options_t;

typedef struct 
  {
  List_t param;
  Input_t  input;
  Output_t *output;
  Output_t *errors;
  Options_t *options;
  StringBuffer_t condStk;
} State_t;

/* }}} */


/*****************************************************************************
 **  Various string functions (used to be in stringproc.h)
 *****************************************************************************/

/* Copies a string into newly allocated memory.
*/   
static __inline__ char *copyStr(const char *original)
{
char *copy = (char *) malloc( sizeof(char) * (strlen(original)+1) );
if( copy != NULL )
   strcpy(copy,original);   
return copy;
}


/* Appends the two strings and puts the result into newly allocated memory
*/
static __inline__ char *appendStr(const char *str1, const char *str2)
{
char *fullpath = 
    (char *) malloc( (strlen(str1)+ strlen(str2) + 1) * sizeof(char));
if( fullpath != NULL )
    {
    strcpy(fullpath,str1);
    strcpy(&fullpath[strlen(fullpath)],str2);
    }
return fullpath;
}


#ifdef HAVE_STRCASECMP
#include <strings.h>
#else
#define makeCharLowercase(ch) (tolower((ch)))
#endif


/* Returns 0 if strings are identical, and 0 otherwise. This is a cut down
   version of POSIX's strcasecmp(), which in turn is a variant of C89's
   strcmp(). As an optimisation the nature of the difference is not
   calculated. Maybe i'm being over-pessimistic about lack of support
   for strcasecmp()..
   FIXME: For cmd parsing, use table and 1 copy..
*/
static __inline__ int compareStrings(const char *str1, const char *str2)
{
#ifdef HAVE_STRCASECMP
return strcasecmp(str1,str2);
#else
while( *str1 && *str2 )
   {
   if( makeCharLowercase(*str1) != makeCharLowercase(*str2) )
      return 1;
   str1++;
   str2++;
   }
if( *str1 || *str2 )
   return 1;
return 0;
#endif
}


/*****************************************************************************
 ** Other headers included in all C files. These all contain inlined functions
 ** that require the typedefs defined above, so (unusually) they have to be
 ** at the end (rather than the start) of this file. Not all the C files need
 ** all (or all parts of) header files, but an arrangment that was closer
 ** to actual needs proved to be a dependency headache.
 *****************************************************************************/

#include "input.h"

#include "output.h"
