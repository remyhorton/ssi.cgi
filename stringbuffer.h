/*
  stringbuffer.c - Basic String Buffer ADT
  Basically a cut-down C version of Java's string buffer. The functions
  are all in this header as these functions are supposed to always be inlined.
  It is also used as a growing stack of integers.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef struct
  {
  int maxlen;
  int length;
  char *buffer;  
} StringBuffer_t;


#define INIT_STRINGBUFFER_CAPACITY 32

static __inline__ int initStringBuffer(StringBuffer_t *buffer)
{
buffer->maxlen = INIT_STRINGBUFFER_CAPACITY;
buffer->length = 0;
buffer->buffer = (char *) malloc( buffer->maxlen * sizeof(char) );
if( buffer->buffer == NULL )
    return 0;
return 1;
}


static __inline__ void resetStringBuffer(StringBuffer_t *buffer)
{
buffer->length = 0;
}


static __inline__ int appendStringBuffer(StringBuffer_t *buffer, const char c)
{
char *newbuffer;
if( buffer->length >= buffer->maxlen )
   {
   buffer->maxlen = buffer->maxlen << 1;
   newbuffer = (char *)realloc( buffer->buffer, buffer->maxlen*sizeof(char) );
   if( newbuffer == NULL )
      return 0;
   buffer->buffer = newbuffer;
   }
buffer->buffer[buffer->length] = c;
buffer->length++;
return 1;
}


static __inline__ char *getStringBufferString(StringBuffer_t *buffer)
{
appendStringBuffer(buffer, 0);
return buffer->buffer;
}


static __inline__ int sbPush(StringBuffer_t *buffer, const int value)
{
assert( value < 128 && value > -128 );
return appendStringBuffer(buffer, (const char)value);
}


static __inline__ int sbPeek(StringBuffer_t *buffer)
{
return buffer->buffer[buffer->length-1];
}

static __inline__ int sbPop(StringBuffer_t *buffer)
{
int value = buffer->buffer[buffer->length-1];
assert( buffer->length > 1 );
buffer->length--;
return value;
}
