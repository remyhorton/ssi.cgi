/*
  pairlist.h - List handling functions

  Previously there were two simular key/value pair modules - one for
  string values, and one for integers. This combined module replaces 
  both of them. Most of the inlining has also been removed, partly to make
  the code cleaner, and partly to reduce code size.

  This file is part of SSI.cgi
  SSI.cgi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  SSI.cgi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with SSI.cgi.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef struct ListLink_s ListLink_t;
struct ListLink_s
  {
  char blockBase;
  char *key;
  char *value;
  struct ListLink_s *next;
};

typedef struct 
  {
  char **memory;
  char **memoryTail;
  ListLink_t *anchor;
  ListLink_t *tail;
  ListLink_t *unused;
  int count;
  int nextint;
} List_t;


int  listInit  (List_t *list);
void listDelete(List_t *victim, const int freeValues);
void listReset (List_t *list, const int freeValues);
int  listFetchId(List_t *list, char *key);
int  listAddStr(List_t *list, char *key, char *value);
ListLink_t *listFetch(List_t *list, char *key);

static inline int listGetCount(List_t *list)
{
return list->count;
}

